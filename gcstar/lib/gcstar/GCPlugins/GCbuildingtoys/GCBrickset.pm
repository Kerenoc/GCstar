package GCPlugins::GCbuildingtoys::GCBrickset;

###################################################
#
#  Copyright 2005-2016 Tian
#  Copyright 2022-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCPluginsBase;

{
    package GCPlugins::GCbuildingtoys::GCPluginBrickset;

    use base qw(GCPluginParser);

    use JSON qw( decode_json );

    sub parse
    {
        my ($self, $html) = @_;

        if ($self->{parsingList})
        {
            $self->{result} = decode_json($html);
            for my $item (@{$self->{result}->{sets}})
            {
                $self->{itemIdx}++;
                $self->{itemsList}[$self->{itemIdx}]->{reference} = $item->{number};
                $self->{itemsList}[$self->{itemIdx}]->{name} = $item->{name};
                $self->{itemsList}[$self->{itemIdx}]->{released} = $item->{year};
                $self->{itemsList}[$self->{itemIdx}]->{nbpieces} = $item->{pieces};
                $self->{itemsList}[$self->{itemIdx}]->{theme} = $item->{theme};
                $self->{itemsList}[$self->{itemIdx}]->{mainpic} = $item->{image}->{imageURL};
                $self->{itemsList}[$self->{itemIdx}]->{brand} = 'Lego';
                $self->{itemsList}[$self->{itemIdx}]->{url} = $item->{bricksetURL};
                $self->{index}->{$item->{bricksetURL}} = $self->{itemIdx};
            }
        }
        else
        {
            # JSON already parsed
            return if (! $html =~ m|local://(\d+)|);
            $html =~ s|.*local://||;
            my $index = $self->{index}->{$html};
            my $item = $self->{result}->{sets}[$index];
            $self->{curInfo}->{name} = $item->{name};
            $self->{curInfo}->{mainpic} = $item->{image}->{imageURL};
            $self->{curInfo}->{reference} = $item->{number};
            $self->{curInfo}->{released} = $item->{year};
            $self->{curInfo}->{ratingpress} = 2.0 * $item->{rating};
            $self->{curInfo}->{theme} = $item->{theme};
            $self->{curInfo}->{theme} .= " - ".$item->{subtheme}
                if $item->{subtheme};
            $self->{curInfo}->{ean} = $item->{barcode}->{EAN};
            $self->{curInfo}->{brand} = 'Lego';
            $self->{curInfo}->{nbpieces} = $item->{pieces};
            $self->{curInfo}->{nbfigs} = $item->{minifigs};
            $self->{curInfo}->{setID} = $item->{setID};
            $self->{curInfo}->{web} = $html;
        }
        return $html;
    }

    sub getAdditionalImages
    {
        my $self = shift;
        my $url = $self->{rootUrl}."getAdditionalImages?apiKey=".$self->{apiKey} 
            . "&setID=".$self->{curInfo}->{setID};
        $self->{parsingTips} = 1;
        my $content = $self->loadPage($url, 0, 1);
        my $result = decode_json($content);
        for my $item (@{$result->{additionalImages}})
        {
            push @{$self->{curInfo}->{images}}, $item->{thumbnailURL};
            if (! exists $self->{curInfo}->{mainpic})
            {
                $self->{curInfo}->{mainpic} = $item->{imageURL};
            }
        }
        return;
    }

    sub getSearchFieldsArray
    {
        return ['reference', 'name'];
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{index} = {};
        $self->{hasField} = {
            reference => 1,
            name => 1,
            mainpic => 0,
            nbpieces => 1,
            theme => 1,
            released => 1
        };
        $self->{apiKey} = "3-rYND-tcJG-7jsvi";
        $self->{rootUrl} = "https://brickset.com/api/v3.asmx/";

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        return $html;
    }

    sub decodeEntitiesWanted
    {
        return 0;
    }

    sub getItemInfo
    {
        my $self = shift;

        $self->SUPER::getItemInfo;
        $self->getAdditionalImages;

        return $self->{curInfo};
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;
        $word =~ s/\+/ /g;
        return $self->{rootUrl}."getSets?apiKey=".$self->{apiKey} 
            . "&userHash=&params={\"query\":\"$word\"}";
    }

    sub getItemUrl
    {
        my ($self, $word) = @_;

        return 'local://'.$word;
    }

    sub getName
    {
        return 'Brickset';
    }

    sub getAuthor
    {
        return 'Tian - Kerenoc';
    }

    sub getLang
    {
        return 'EN';
    }

    sub isPreferred
    {
        return 1;
    }
}

1;
