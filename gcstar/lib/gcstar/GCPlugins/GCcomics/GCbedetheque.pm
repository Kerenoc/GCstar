package GCPlugins::GCcomics::GCbedetheque;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2016-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCcomics::GCcomicsCommon;

{
    package GCPlugins::GCcomics::GCPluginbedetheque;

    use base qw(GCPlugins::GCcomics::GCcomicsPluginsBase);

    use constant DONE => 2;

    sub getSearchUrl
    {
        my ( $self, $word ) = @_;

        my $token="" ;

        if ($self->{searchField} eq 'writer')
        {
            return "https://www.bedetheque.com/search/tout?RechTexte=$word&RechWhere=7";
         }
        elsif ($self->{searchField} eq $self->{seriesField} )
        {
            $word =~ s/ *\(.*\)//;
            return "http://www.bedetheque.com/search/albums?RechSerie=$word";
        }
        else
        {
            return "https://www.bedetheque.com/search/tout?RechTexte=$word";
        }
    }

    sub getSearchFieldsArray
    {
        return ['series', 'writer', 'title'];
    }

    sub getItemUrl
    {
        my ( $self, $url ) = @_;
        my @array = split( /#/, $url );
        $self->{site_internal_id} = $array[1];

        return $url if $url =~ /^https?:/;
        return "http://www.bedetheque.com/" . $url;
    }

    sub getNumberPasses
    {
        return 1;
    }

    sub getName
    {
        return "Bedetheque";
    }

    sub getAuthor
    {
        return 'Mckmonster - Jpa31 - Kerenoc';
    }

    sub getLang
    {
        return 'FR';
    }

    sub getSearchCharset
    {
        my $self = shift;

        # Need urls to be double character encoded
        return "utf8";
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless( $self, $class );

        $self->{ua}->default_header('Referer' => 'http://www.bedetheque.com');

        $self->initFieldNames();

        $self->{hasField} = {
            $self->{seriesField} => 1,
            title  => 1,
            volume => 1,
          };

        $self->{last_cover}       = "";
        $self->{site_internal_id} = "";
        $self->{serie}            = "";
        $self->{current_field}    = "";

        return $self;
    }

    sub preProcess
    {
        my ( $self, $html ) = @_;

        $self->{parsingEnded} = 0;
        $html =~ s/\s+/ /g;
        $html =~ s/\r?\n//g;

        if ( $self->{parsingList} )
        {
            if ( $html =~ m/(\d+\salbum\(s\).+)/ ) {

                #keep only albums, no series or objects
                $html = $1;
                $self->{alternative} = 0;
            } elsif ( $html =~ m/(<div id="albums_table">.+)/ ) {
                $html = $1;
                $self->{alternative} = 1;
            }
            $self->{isResultsTable}   = 0;
            $self->{itemIdx}          = -1;
        }
        else
        {
            $html =~ m/(<div class="bandeau-principal">.+)/;
            $html = $1;
            $self->{isResultsTable} = 0;
            $self->{parsingEnded}   = 0;
            $self->{isCover} = 0;
            $self->{isTabs} = 0;
            $self->{isLabel} = 0;
            $self->{itemIdx}++;
        }

        if ($self->{loadedUrl} !~ m/\/serie-/)
        {
	        foreach my $item( ( 'Colourist', 'Illustrator', 'ISBN', 'Numberboards', 'Publisher',
	                            'Collection', 'Serie', 'Title', 'Synopsis', 'Writer', 'Number',
	                            'Volume', 'Cover', 'Cost', 'Label', 'Format', 'Publishdate' ) )
	        {
	            $self->{"is$item"} = 0;
	        }        	
        }
        else
        {
        	$self->{isSynopsis} = 0;
        }

        return $html;
    }

    sub start
    {
        my ( $self, $tagname, $attr, $attrseq, $origtext ) = @_;

        return if ( $self->{parsingEnded} );

        if ( $self->{parsingList} )
        {
            if ( !defined ($self->{alternative}) || (!$self->{alternative}) )
            {
                if ( ( $tagname eq "a" ) && ( $attr->{href} =~ m/www.bedetheque.com\/BD-/ ) ) {
                    $self->{isCollection} = 1;
                    $self->{itemIdx}++;
                    $self->{isVolume} = 1;

                    my $searchUrl =  substr($attr->{href},0,index($attr->{href},".")).substr($attr->{href},index($attr->{href},"."));
                    $self->{itemsList}[$self->{itemIdx}]->{url} = $searchUrl;
                    if ($searchUrl =~ /\/BD-(.*)-Tome-(\d+)(.*)-\d+.html/ )
                    {
                        $self->{itemsList}[ $self->{itemIdx} ]->{$self->{seriesField}}= $1;
                        $self->{itemsList}[ $self->{itemIdx} ]->{volume}= $2;
                        $self->{itemsList}[ $self->{itemIdx} ]->{title} = $3;
                    }
                    $self->{itemsList}[$self->{itemIdx}]->{title} .= $attr->{title}
                        if $attr->{title} && $attr->{title} ne 'tooltip';
                }
                elsif (( $tagname eq "ul" ) && $attr->{class} && ( $attr->{class} eq "search-list" || $attr->{class} eq "nav-liste") ) {
                    $self->{inTable} = 1;
                }
                elsif ( ($self->{isVolume}) && ( $tagname eq "a" ) && ( $attr->{title} eq "tooltip" ) ) {
                    $self->{itemsList}[$self->{itemIdx}]->{image} = $attr->{rel};
                    $self->{itemsList}[$self->{itemIdx}]->{url} = $attr->{href};
                }
                elsif ( ($self->{isVolume}) && ( $tagname eq "span" ) && ( $attr->{class} eq "titre" || $attr->{class} eq "libelle" || $attr->{class} eq "highlight") ) {
                    $self->{isTitle} = 1;
                }
                elsif ( ($self->{isVolume}) && ( $tagname eq "span" ) && ( $attr->{class} eq "serie" ) ) {
                    $self->{isSerie} = 1;
                }
                elsif ( ($self->{isVolume}) && ( $tagname eq "span" ) && ( $attr->{class} eq "num" ) ) {
                    $self->{isNumber} = 1;
                }
            } else {
                if ( ( $tagname eq "ul" ) && ( $attr->{class} eq "search-list" || $attr->{class} eq "nav-list") ) {
                    $self->{inTable} = 1;
                }
                elsif ( ($self->{inTable}) && ( $tagname eq "li" ) ) {
                    $self->{itemIdx}++;
                    $self->{isVolume} = 1;
                }
                elsif ( ($self->{isVolume}) && ( $tagname eq "a" ) && ( $attr->{title} eq "tooltip" ) ) {
                    $self->{itemsList}[$self->{itemIdx}]->{image} = $attr->{rel};
                    $self->{itemsList}[$self->{itemIdx}]->{url} = $attr->{href};
                }
                elsif ( ($self->{isVolume}) && ( $tagname eq "span" ) && ( $attr->{class} eq "titre" || $attr->{class} eq "libelle") ) {
                    $self->{isTitle} = 1;
                }
                elsif ( ($self->{isVolume}) && ( $tagname eq "span" ) && ( $attr->{class} eq "serie" ) ) {
                    $self->{isSerie} = 1;
                }
            }
        }
        else
        {
            if ( ( $self->{isCover} == 0 ) && ( $tagname eq "a" ) && ( $attr->{href} =~ m/Couvertures\/.*\.[jJ][pP][gG]/ ) ) {
                $self->{curInfo}->{image} = $attr->{href};
                $self->{isCover} = 1;
            }
            elsif ( $tagname eq "img" && $attr->{src} =~ /_versos/ )
            {
                $self->{curInfo}->{backpic} = $attr->{src};
            }
            elsif ( $tagname eq "label" ) {
                $self->{isLabel} = 1;
            }
            elsif ( ( $tagname eq "ul" ) && $attr->{class} && ( $attr->{class} eq "tabs-album" ) && $self->{isSerie} ) {
                $self->{isTabs} = 1;
            }
            elsif ( ( $tagname eq "a" ) && $attr->{itemprop} && ( $attr->{itemprop} eq "url" ) && ( $self->{isTitle} ne DONE) ) {
                # $self->{isTitle} = 1;
                $self->{curInfo}->{title} = $attr->{title}
                    if ! $self->{curInfo}->{title};
                $self->{isTitle}   = DONE ;
            }
            elsif ( ( $tagname eq "a" ) && $attr->{href} && ($attr->{href} =~ m|www.bedetheque.com/serie-|) && $self->{isSerie} ne DONE ) {
                $self->{perhapsseries} = $attr->{title} ;
                $self->{curInfo}->{nextUrl} = $attr->{href}
                    if ($self->{loadedUrl} =~ m|/BD| );
            }
            elsif ( ( $tagname eq "div" ) && $attr->{class} && ( $attr->{class} eq "bandeau-menu" ) && $self->{isSerie} ne 2 && $self->{isTabs} ) {
                $self->{isSerie} = 1;
            }
            elsif ( ( $tagname eq "span" ) && $attr->{itemprop} && ( $attr->{itemprop} eq "author" ) && $self->{isWriter} ne DONE ) {
                $self->{isWriter} = 1;
            }
            elsif ( ( $tagname eq "span" ) && $attr->{itemprop} && ( $attr->{itemprop} eq "illustrator" ) && ( $self->{isIllustrator} ne DONE ) ) {
                $self->{isIllustrator} = 1;
            }
            elsif ( ( $tagname eq "span" ) && $attr->{itemprop} && ( $attr->{itemprop} eq "illustrator" ) && ( $self->{isColourist} ne DONE ) && ( $self->{isIllustrator} eq DONE) ) {
                $self->{isColourist} = 1;
            }
            elsif ( ( $tagname eq "span" ) && $attr->{itemprop} && ( $attr->{itemprop} eq "publisher" ) && ( $self->{isPublisher} ne DONE ) ) {
                $self->{isPublisher} = 1;
            }
            elsif ( ( $tagname eq "span" ) && $attr->{class} && ( $attr->{class} eq "collection" ) && ( $self->{isCollection} ne DONE ) ) {
                $self->{isCollection} = 1;
            }
            elsif ( ( $tagname eq "span" ) && $attr->{itemprop} && ( $attr->{itemprop} eq "isbn" ) && ( $self->{isISBN} ne DONE) ) {
                $self->{isISBN} = 1;
            }
            elsif ( ( $tagname eq "span" ) && $attr->{itemprop} && ( $attr->{itemprop} eq "numberOfPages" ) && ( $self->{isNumberboards} ne DONE) ) {
                $self->{isNumberboards} = 1;
            }
            elsif ( ( $tagname eq "ul" ) && $attr->{class} && ( $attr->{class} eq "serie-info" ) && ( $self->{isSynopsis} ne DONE ) ) {
                $self->{isSynopsis} = 1;
            }
            elsif ( ( $tagname eq "p" ) && $self->{isSynopsis} eq 1 ) {
            	$self->{isSynopsis} = 3;
            }
            elsif ( ( $tagname eq "meta" ) && $attr->{itemprop} && ( $attr->{itemprop} eq "alternativeheadline" ) && ( $self->{isVolume} ne DONE ) ) {
                $self->{curInfo}->{volume} = $attr->{content};
                $self->{curInfo}->{volume} =~ s/Tome(\s)*//gsi;
                $self->{isVolume} = DONE ;
            }
            elsif ( ( $tagname eq "ul" ) && $attr->{class} && ( $attr->{class} eq "liste-albums" ) ) {
                $self->{isColourist} = DONE; # To avoid getting mess with illustrator
            }
            elsif ( ( $tagname eq "br" ) && $self->{isSynopsis} eq 3 )
            {
            	$self->{curInfo}->{$self->{descriptionField}} .= "\n\n";   	
            }
        }
    }

    sub text
    {
        my ( $self, $origtext ) = @_;

        $origtext =~ s/ *$//;
        return if ( $origtext eq '' );
        return if ( $self->{parsingEnded} );

        if ( $self->{parsingList} )
        {
            if ( !defined ($self->{alternative}) || (!$self->{alternative}) ) {
                if ( $self->{isSerie} == 1)
                {
                    $self->{itemsList}[ $self->{itemIdx} ]->{$self->{seriesField}}  = $origtext;
                    $self->{isSerie} = 0;
                }
                elsif ( $self->{isTitle} == 1)
                {
                    $self->{itemsList}[ $self->{itemIdx} ]->{title} = $origtext;
                    # $self->{isTitle} = 0;
                }
                elsif ( $self->{isNumber} == 1)
                {
                    $self->{itemsList}[ $self->{itemIdx} ]->{volume} = $origtext;
                    $self->{itemsList}[ $self->{itemIdx} ]->{volume} =~ s/#//;
                    $self->{isNumber} = 0;
                }
                elsif ($self->{isCollection} == 1)
                {
                    #sometimes the field is "-vol-title", sometimes "--vol-title"
                    $origtext =~ s/-+/-/;
                    if ( $origtext =~ m/(.+)\s-(\d+)-\s(.+)/ ) {
                        $self->{itemsList}[ $self->{itemIdx} ]->{$self->{seriesField}} = $1;
                        $self->{itemsList}[ $self->{itemIdx} ]->{volume} = $2;
                    }
                    elsif ( $origtext =~ m/(.*)-(\d+)-(.+)/ ) {
                        $self->{itemsList}[ $self->{itemIdx} ]->{title} = $3;
                        $self->{itemsList}[ $self->{itemIdx} ]->{volume} = $2;
                    }
                    elsif ( $origtext =~ /-/ ){
                        my @fields = split( /-/, $origtext );
                        $self->{itemsList}[ $self->{itemIdx} ]->{$self->{seriesField}} = $fields[0];
                        $self->{itemsList}[ $self->{itemIdx} ]->{volume} = $fields[1];
                    }
                    $self->{isCollection} = 0;
                }
            } else {
                if ( ( $self->{inTable} ) && ( $self->{isTitle} ) ) {
                    $self->{itemsList}[ $self->{itemIdx} ]->{title} = $origtext;
                } elsif ( ( $self->{inTable} ) && ( $self->{isVolume} ) ) {
                    $self->{itemsList}[ $self->{itemIdx} ]->{volume} = $origtext;
                }
            }
        }
        else
        {
            if ( $self->{isResultsTable} == 1 )
            {
                $origtext=~s/:\s+/:/;
                my %td_fields_map = (
                    "Identifiant :"   => '',
                    "Scénario :"      => 'writer',
                    "Dessin :"        => 'illustrator',
                    "Couleurs :"      => 'colourist',
                    "Dépot légal :"   => 'publishdate',
                    "Achevé impr. :"  => 'printdate ',
                    "Estimation :"    => 'cost',
                    "Editeur :"       => 'publisher',
                    "Collection : "   => 'collection',
                    "Taille :"        => 'format',
                    "ISBN :"          => 'isbn',
                    "Planches :"      => 'numberboards'
                  );

                if ( ( $self->{openlabel} ) && ( exists $td_fields_map{$origtext} ) ) {
                    $self->{current_field} = $td_fields_map{$origtext};
                }
                elsif ( defined ( $self->{current_field} ) && ( $self->{current_field} !~ /^$/ ) )
                {
                    $origtext=~s/&nbsp;/ /g;
                    $origtext=~s/\s+$//g;
                    $self->{curInfo}->{$self->{current_field}} = $origtext;
                    $self->{current_field} = "";
                }
            }
            elsif ( $origtext =~ /La\sS.rie/gsi && ! $self->{curInfo}->{name})
            {
                $self->{curInfo}->{$self->{seriesField}} = $self->{perhapsseries};
                $self->{curInfo}->{$self->{seriesField}} =~s/^\s+//;
                $self->{curInfo}->{name} = $self->{curInfo}->{$self->{seriesField}}." ";
                $self->{curInfo}->{name} .= sprintf("#%03d",$self->{curInfo}->{volume})." " if ($self->{curInfo}->{volume});
                $self->{curInfo}->{name} .= $self->{curInfo}->{title};

                $self->{isSerie}   = DONE ;
                $self->{isTabs}    = 0 ;
            }
            elsif ( $self->{isWriter} eq 1 ) {
                $self->{curInfo}->{$self->{authorField}} = $origtext;
                $self->{isWriter}   = DONE ;
            }
            elsif ( $self->{isIllustrator} eq 1 ) {
                $self->{curInfo}->{$self->{illustratorField}} = $origtext;
                $self->{isIllustrator}   = DONE ;
            }
            elsif ( $self->{isColourist} eq 1 ) {
                $self->{curInfo}->{colourist} = $origtext;
                $self->{isColourist}   = DONE ;
            }
            elsif ( $self->{isPublisher} eq 1 ) {
                $self->{curInfo}->{publisher} = $origtext;
                $self->{isPublisher}   = DONE ;
            }
            elsif ( $self->{isCollection} eq 1 ) {
                $origtext =~ s/^\(?(.*)\)$/$1/;
                $self->{curInfo}->{$self->{collectionField}} = $origtext;
                $self->{isCollection}   = DONE ;
            }
            elsif ( $self->{isISBN} eq 1 ) {
                $self->{curInfo}->{isbn} = $origtext;
                $self->{isISBN}   = DONE ;
            }
            elsif ( $self->{isNumberboards} eq 1 ) {
                $self->{curInfo}->{$self->{nbpagesField}} = $origtext;
                $self->{isNumberboards}   = DONE ;
            }
            elsif ( ( $self->{isLabel} ) && ( $origtext =~ m/Dépot légal/ ) && ( $self->{isPublishdate} ne DONE ) ) {
                $self->{isPublishdate} = 1 ;
                $self->{isLabel} = 0 ;
            }
            elsif ( $self->{isPublishdate} eq 1 ) {
                my @publishDate = split m|/|, $origtext;
                $origtext = '01/'.$origtext if (scalar(@publishDate) eq 2);
                $self->{curInfo}->{$self->{publicationField}} = $origtext;
                $self->{isPublishdate}   = DONE ;
            }
            elsif ( ( $self->{isLabel} ) && ( $origtext =~ m/Estimation/ ) && ( $self->{isCost} ne DONE ) ) {
                $self->{isCost}  = 1 ;
                $self->{isLabel} = 0 ;
            }
            elsif ( $self->{isCost} eq 1 && $origtext =~ m/(\d+)/ ) {
                $self->{curInfo}->{cost} = $1;
                $self->{isCost}   = DONE ;
            }
            elsif ( ( $self->{isLabel} ) && ( $origtext =~ m/Format/ ) && ( $self->{isFormat} ne DONE ) ) {
                $self->{isFormat} = 1 ;
                $self->{isLabel} = 0 ;
            }
            elsif ( $self->{isFormat} eq 1) {
                $self->{curInfo}->{format} = $origtext;
                $self->{isFormat}   = DONE ;
            }
            elsif ( $self->{isSynopsis} eq 3) {
                $self->{curInfo}->{$self->{descriptionField}} .= $origtext;
                $self->{curInfo}->{$self->{descriptionField}} =~ s/^(\s)*//;
                $self->{curInfo}->{$self->{descriptionField}} =~ s/(\s)*$//;
            }
        }
    }

    sub end
    {
        my ( $self, $tagname ) = @_;

        return if ( $self->{parsingEnded} );

        if ( $self->{parsingList} )
        {
            if ( ( $self->{inTable} ) && ( $tagname eq "span" ) ) {
                $self->{isTitle} = 0;
                $self->{isNumber} = 0;
                $self->{isSerie} = 0;
            } elsif ( ( $self->{inTable} ) && ( $tagname eq "li" ) ) {
                $self->{isVolume} = 0;
            } elsif ( !defined ($self->{alternative}) || (!$self->{alternative}) ) {
                if ( ( $tagname eq "i" ) && $self->{isCollection} == 1)
                {
                    #end of collection, next field is title
                    $self->{isTitle}      = 1;
                    $self->{isCollection} = 0;
                }
                elsif ( $tagname eq 'a' )
                {
                     $self->{isTitle} = 0;
                     $self->{isVolume} = 0;
                }
            }
        }
        else
        {
            if ( ( $tagname eq "ul" ) && $self->{isResultsTable} == 1 )
            {
                $self->{isIssue} = 0;
                $self->{isResultsTable} = 0;
            }
            elsif ( $tagname eq "label" ) {
                $self->{openlabel} = 0;
                $self->{isLabel} = 0;
            }
            elsif ($tagname eq "p" && $self->{isSynopsis} eq 3)
            {
            	$self->{isSynopsis} = DONE;
            }
        }
    }
}

1;
