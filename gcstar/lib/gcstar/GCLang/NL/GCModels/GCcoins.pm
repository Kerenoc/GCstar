{
    package GCLang::NL::GCModels::GCcoins;

    use utf8;
###################################################
#
#  Auteursrecht 2005-2009 Tian
#
#  Dit bestand is onderdeel van GCstar.
#
#  GCstar is gratis software; je kan het verspreiden en/ of wijzigen
#  onder de voorwaarden van de GNU General Public License zoals gepubliceerd door
#  de Free Software Foundation; ofwel versie 2 van de licentie, of
#  (op uw keuze) een latere versie.
#
#  GCstar wordt verspreid in de hoop dat het nuttig zal zijn
#  maar ZONDER ENIGE GARANTIE, zelfs zonder de impliciete garantie van
#  Verkoopbaarheid of geschiktheid voor een bepaald doel. Zie de
#  GNU General Public License voor meer details.
#
#  Je zou een kopie van de GNU General Public License moeten ontvangen hebben
#  samen met GCstar; zo niet, schrijf naar de Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################
    
    use strict;
    use base 'Exporter';

    our @EXPORT = qw(%lang);

    our %lang = (
    
        CollectionDescription => 'Numismatische collectie',
        Items => {0 => 'Muntstuk',
                  1 => 'Muntstuk',
                  X => 'Muntstukken'},
        NewItem => 'Nieuw muntstuk',

        Name => 'Naam',
        Country => 'Land',
        Year => 'Jaar',
        Calendar => 'Calendar',
        Currency => 'Valuta',
        Value => 'Waarde',
        Picture => 'Hoofdafbeelding',
        Diameter => 'Diameter',
        Metal => 'Metaal',
        Weight => 'Weight (g)',
        Depth => 'Depth (mm)',
        Edge => 'Rand',
        Edge1 => 'Rand 1',
        Edge2 => 'Rand 2',
        Edge3 => 'Rand 3',
        Edge4 => 'Rand 4',
        Head => 'Kop',
        Tail => 'Munt',
        Front => 'Front',
        Back => 'Back',
        Comments => 'Opmerkingen',
        History => 'Geschiedenis',
        Website => 'Webpagina',
        Estimate => 'Raming',
        References => 'Referenties',
        Type => 'Type',
        Coin => 'Muntstuk',
        Banknote => 'Bankbiljet',
        Pattern => "Ontwerp",
        Fantasy => "Fantasie-geld",
        "Anniversary coin" => 'Herdenkingsmunt',
        "Non circulating coin" => 'Niet-circulerende munt',
        Token => 'Token',
        
        Main => 'Hoofd',
        Description => 'Beschrijving',
        Other => 'Overige informatie',
        Pictures => 'Afbeeldingen',
        
        Condition => 'Toestand (PCGS)',
        Grade1  => 'BS-1',
        Grade2  => 'FR-2',
        Grade3  => 'AG-3',
        Grade4  => 'G-4',
        Grade6  => 'G-6',
        Grade8  => 'VG-8',
        Grade10 => 'VG-10',
        Grade12 => 'F-12',
        Grade15 => 'F-15',
        Grade20 => 'VF-20',
        Grade25 => 'VF-25',
        Grade30 => 'VF-30',
        Grade35 => 'VF-35',
        Grade40 => 'XF-40',
        Grade45 => 'XF-45',
        Grade50 => 'AU-50',
        Grade53 => 'AU-53',
        Grade55 => 'AU-55',
        Grade58 => 'AU-58',
        Grade60 => 'MS-60',
        Grade61 => 'MS-61',
        Grade62 => 'MS-62',
        Grade63 => 'MS-63',
        Grade64 => 'MS-64',
        Grade65 => 'MS-65',
        Grade66 => 'MS-66',
        Grade67 => 'MS-67',
        Grade68 => 'MS-68',
        Grade69 => 'MS-69',
        Grade70 => 'MS-70',
    
        Axis => 'Axis',
        Medal => 'Medal',
        Monetary => 'Monetary (180º)',
        Turn => 'Turn',
        "Years of coinage" => 'Years of coinage',
        Quantity => 'Quantity',
        Mint => 'Mint',
        "Mint mark" => 'Mint mark',
        Mintmaster => 'Mintmaster',
        "Mintmaster mark" => 'Mintmaster mark',
        City => 'City',
        "City letter" => 'City letter',
        Series => 'Series',
        "Edge type" => 'Edge type',
        Catalogue1 => '1st Catalogue',
        Number1 => '1st Number',
        Catalogue2 => '2nd C',
        Number2 => '2nd N',
        Catalogue3 => '3rd C',
        Number3 => '3rd N',
        Estimate2 => '2 R',
        Estimate3 => '3 R',
        Number => 'Number',
        Form => 'Form',
        Buyed => 'Bought',
        Found => 'Found',
        Bring => 'Bring',
        Gift => 'Gift',
        Location => 'Location',
        Price => 'Price',
        
     );
}

1;
