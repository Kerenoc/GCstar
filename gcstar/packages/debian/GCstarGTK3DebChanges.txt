Details of changes made to files in GCstar-Gtk3/gcstar/packages/debian/ folder :

-Compatibility level set to 10 in compat file (former value of 7 will be deprecated)
-Changes in patch file for debian 04-install-set_usr_lib.dpatch to match changes in install file 
-Changes in patch file for debian 07_fix_manpath.dpatch to match changes in install file
-Changes in patch file for debian 02gzip-manpage.dpatch to match changes in install file
-Changes in patch file for debian 03_change_default_browser.dpatch to match changes in install file
-"dh_clean -k" substituted by "dh_prep" in rules file (dh_clean -k deprecated)
-Version set to 1.8.0 and ubuntu version to focal fossa in changelog file
-Removed references to old and deprecated gcfilms package in control file


Modified files are provided in the issue description.


Building of deb file (dpkg-deb package needs to be installed first):

Copy debian folder (in GCstar-Gtk3/gcstar/packages/ folder) to GCstar-Gtk3/gcstar/
In GCstar-Gtk3/gcstar/ folder, the following command will build gcstar_1.8.0-1ubuntu1_all.deb in GCstar-Gtk3/ folder 
dpkg-buildpackage -rfakeroot -b -uc -us

Manual install of deb package :
sudo dpkg -i gcstar_1.8.0-1ubuntu1_all.deb

To repair missing dependencies after installation :
sudo apt-get install -f



Varkolak
