package GCPlugins::GCgames::GCMobyGames;

###################################################
#
#  Copyright 2005-2023 Christian Jodar - TPF - Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCgames::GCgamesCommon;

{
    package GCPlugins::GCgames::GCPluginMobyGames;

    use base 'GCPlugins::GCgames::GCgamesPluginsBase';
    use HTML::Entities;

    sub extractTips
    {
        my ($self, $html_ini) = @_;
        my $answer = "";
        my $html = $self->loadPage($html_ini, 0, 1);
        $html =~ s|<ul>||gi;
        $html =~ s|</ul>||gi;
        $html =~ s|<b>||gi;
        $html =~ s|</b>||gi;
        $html =~ s|<li>||gi;
        $html =~ s|</li>||gi;
        $html =~ s|</h3>||gi;
        $html =~ s|<hr />||gi;
        $html =~ s|<br>|\n|gi;
        $html =~ s|<p>|\n|gi;
        $html =~ s|</p>||gi;
        $html =~ s|<pre>||gi;
        $html =~ s|</pre>||gi;
        $html =~ s|</td>||gi;
        $html =~ s|</tr>||gi;
        my $found = index($html,"sbR sbL sbB\">");
        if ( $found >= 0 )
        {
           $answer = substr($html, $found + length('sbR sbL sbB">'),length($html)- $found -length('sbR sbL sbB">') );
           $answer = substr($answer, 0, index($answer,"</table>"));

           $answer = decode_entities($answer);
        }

        return $answer;
    }

    sub extractPlayer
    {
        my ($self, $html_ini, $word) = @_;
        my $html = 0;
        my $found = index($html_ini,$word);
        if ( $found >= 0 )
        {
           $html = substr($html_ini, $found + length($word),length($html_ini)- $found -length($word) );
           $html = substr($html,0, index($html,"</a>") );
           $html = reverse($html);
           $html = substr($html,0, index($html,">") );
           $html = reverse($html);
           $html =~ s/&nbsp;/ /g;
           $html =~ s/1 Player/1/;
        }
        return $html;
    }

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        if ($self->{parsingList})
        {
            if ( $self->{insideGame}
              && ($tagname eq 'a')
              && ($attr->{href} =~ m|/game/[0-9]| ) )
            {
                # Test if there is a platform name in it
                # (i.e. if we can find a second slash after game/ )
                if ($attr->{href} =~ m|/game/[^/]*/|)
                {
                    $self->{itemIdx}++;
                    $self->{insideGame} = 0;
                    $attr->{href} = $self->normUrl($attr->{href});
                    $self->{itemsList}[$self->{itemIdx}]->{url} = $attr->{href};
                    $self->{isName} = 1 ;
                }
            }
            elsif ($tagname eq 'br' && $self->{isAka})
            {
                $self->{isAka} = 0;
                $self->{isPlatform} = 1;
            }
        }
        elsif ($self->{parsingTips})
        {
            if (($tagname eq 'table') && ($attr->{summary} =~ m|tip|i))
            {
                $self->{isSectionTips} = 1;
            }
            elsif ( ($tagname eq 'a') && ($self->{isSectionTips}) )
            {
                $self->{tip_wait} = $self->extractTips($self->normUrl($attr->{href}));
            }
            elsif ( ($tagname eq 'b') && ($self->{isSectionTips}) )
            {
                $self->{isType_wait} = 1;
            }
            elsif ($tagname eq 'br')
            {
                $self->{isSectionTips} = 0;
            }
            elsif ($tagname eq 'head')
            {
                $self->{isSectionTips} = 0;
            }
            elsif ($tagname eq 'h2')
            {
                $self->{tip_wait} = 1;
            }
        }
        else
        {
            if ( ($tagname eq 'h1') && ($attr->{class} eq 'mb-0') )
            {
                    $self->{isName} = 1;
            }
            elsif ($tagname eq 'div')
            {
                    if ($attr->{class} =~ m/mobyscore/i)
                    {
                        $self->{isRating} = 1;
                    }
                    elsif ($self->{curInfo}->{genre} && $self->{isGenre})
                    {
                        $self->{isGenre} = 0;
                    }
                    elsif ($attr->{id} eq 'description-text')
                    {
                        $self->{isDescription} = 2;
                    }
            }
            elsif ( ($tagname eq 'a') && ($self->{isEditor}) )
            {
                    $self->{is} = 'editor';
                    $self->{isEditor} = 0;
            }
            elsif ( ($tagname eq 'a') && ($self->{isDeveloper}) )
            {
                    $self->{is} = 'developer';
                    $self->{isDeveloper} = 0;
            }
            elsif ( ($tagname eq 'a') && ($self->{isExclusive}) )
            {
                if ($self->{isExclusive} eq 1)
                {
                    $self->{isExclusive} = $self->{isExclusive} + 1;
                }
                else
                {
                    $self->{curInfo}->{exclusive} = 0;
                    $self->{isExclusive} = 0;
                }
            }
            elsif ( ($tagname eq 'a') && ($self->{isName}) )
            {
                    $self->{is} = 'name';
                    $self->{curInfo}->{platform} = $self->{url_plateforme};
                    $self->{curInfo}->{exclusive} = 1;
                    $self->{isName} = 0;
            }
            elsif ( ($tagname eq 'a') && ($self->{isDate}) )
            {
                    $self->{is} = 'released';
                    $self->{isDate} = 0;
            }
            elsif ( ($tagname eq 'a') && ($self->{isGenre}) )
            {
                    $self->{is} = 'genre';
            }
            elsif ($tagname eq 'img' && ! $self->{curInfo}->{boxpic})
            {
                if ($attr->{class} =~ m/img-box/)
                {
                    $attr->{src} =~ s|/s/|/l/|
                        if $self->{bigPics};
                    $self->{curInfo}->{boxpic} = $self->normUrl($attr->{src});
                    # From here we try to get back cover
                    my $covers = $self->loadPage($self->{rootUrl}.'covers', 0, 1);
                    my $found = index($covers,"Back Cover");
                    if ( $found >= 0 )
                    {
                        $found = index($covers,'src="',$found);
                        if ( $found >= 0 )
                        {
                            my $foundEnd = index($covers,'"',$found+5);
                            $covers = substr($covers, $found+5, $foundEnd-5-$found);
                            $self->{curInfo}->{backpic} = $self->normUrl($covers);
                            $self->{curInfo}->{backpic} =~ s|/s/|/l/|
                                if $self->{bigPics};
                        }
                    }
                }
            }
            elsif ($tagname eq 'html')
            {
                my $html = $self->loadPage($self->{loadedUrl}.'specs', 0, 1);
                my $player_offline = $self->extractPlayer($html, "Number&nbsp;of Offline Players" );
                my $player_online = $self->extractPlayer($html, "Number&nbsp;of Online Players" );
                my $player_total = $self->extractPlayer($html, "Number&nbsp;of Players Supported" );

                if ($player_total)
                {
                   $self->{curInfo}->{players} = $player_total;
                }
                else
                {
                    if ($player_offline)
                    {
                        $self->{curInfo}->{players} = 'Offline: '.$player_offline;
                    }
                    if ($player_online)
                    {
                        if ( $self->{curInfo}->{players} )
                        {
                            $self->{curInfo}->{players} .= '; Online: '.$player_online;
                        }
                        else
                        {
                            $self->{curInfo}->{players} = 'Online: '.$player_online;
                        }
                    }
                }

                $html = $self->loadPage($self->{loadedUrl}.'screenshots', 0, 1);
                my $screen = 1;
                while ($html =~ m|src="(https://cdn.mobygames[^"]*?)"|g)
                {
                    $self->{curInfo}->{'screenshot'.$screen} = $self->normUrl($1);
                    $self->{curInfo}->{'screenshot'.$screen} =~ s|/images/shots/s/|/images/shots/l/|
                        if $self->{bigPics};
                    $screen++;
                    last if $screen > 2;
                }
            }
            elsif ( ($tagname eq 'br') && ($self->{isDescription}) )
            {
                $self->{curInfo}->{description} .= "\n\n";
            }
            elsif ( ($tagname eq 'li') && ($self->{isDescription}) )
            {
                $self->{curInfo}->{description} .= "\n * ";
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

        if ($tagname eq 'tr')
        {
            $self->{isPlatform} = 0;
        }
        elsif ($tagname eq 'section')
        {
            $self->{isDescription} = 0;
        }
        elsif ($tagname eq 'main')
        {
            $self->{tip_wait} = '';
        }
        elsif ( ($tagname eq 'p') && ($self->{isDescription}) )
        {
            $self->{curInfo}->{description} .= "\n\n";
        }

    }

    sub text
    {
        my ($self, $origtext) = @_;

        $origtext =~ s/^[\n ]+//;
        $origtext =~ s/[\n ]+$//;
        return if ! $origtext;

        if ($self->{parsingList})
        {
            if ($origtext eq 'GAME:')
            {
                $self->{insideGame} = 1;
            }
            elsif ($self->{isName})
            {
                $self->{itemsList}[$self->{itemIdx}]->{name} = $origtext;
                $self->{isName} = 0;
                $self->{isDate} = 1;
            }
            elsif ($self->{isPlatform})
            {
                if ($origtext =~ /^AKA:/)
                {
                    $self->{isAka} = 1;
                    $self->{isPlatform} = 0;
                }
                elsif ($origtext !~ /\([0-9]+\)/)
                {
                    if ($self->{itemsList}[$self->{itemIdx}]->{platform})
                    {
                        # multiple platform for one game
                        $self->{itemIdx}++;
                        $self->{itemsList}[$self->{itemIdx}]->{name} = $self->{itemsList}[$self->{itemIdx}-1]->{name};
                        $self->{itemsList}[$self->{itemIdx}]->{released} = $self->{itemsList}[$self->{itemIdx}-1]->{released};
                        $self->{itemsList}[$self->{itemIdx}]->{url} = $self->{itemsList}[$self->{itemIdx}-1]->{url};
                        $self->{itemsList}[$self->{itemIdx}]->{url} =~ s/tpfplatformtpf.*//;
                    }
                    $self->{itemsList}[$self->{itemIdx}]->{platform} = $origtext;
                    $self->{itemsList}[$self->{itemIdx}]->{url} = $self->{itemsList}[$self->{itemIdx}]->{url} . 'tpfplatformtpf' . $self->{itemsList}[$self->{itemIdx}]->{platform};
                }
            }
            elsif ($self->{isDate})
            {
                $self->{itemsList}[$self->{itemIdx}]->{released} = $origtext;
                $self->{isDate} = 0;
                $self->{isPlatform} = 1;
            }
        }
        elsif ($self->{parsingTips})
        {
            if ($self->{isType_wait})
            {
                $self->{type_wait} = $origtext;
                $self->{isType_wait} = 0;
            }
            if ($self->{tip_wait} ne '')
            {
                $self->{isUnlock} = 1 if $origtext =~ /Unlockables/i;
                $self->{isUnlock} = 1 if $origtext =~ /Achievement/i;
                $self->{isCode} = 1 if $origtext =~ /Cheat/i;
                $self->{isCode} = 1 if $self->{type_wait} =~ /Cheat/i;
                if (($self->{isCode}) || ($self->{isUnlock}))
                {
                    # Code trop long si les manip sont decrites completement
                    $self->{tip_wait} =~ s/ the game//;

                    $Text::Wrap::columns = 80;
                    $self->{tip_wait} = Text::Wrap::wrap('', '', $self->{tip_wait});

                    my @array = split(/\n/,$self->{tip_wait});
                    my $element;

                    foreach $element (@array)
                    {
                        if ($element ne '')
                        {
                            $self->{tmpCheatLine} = [];
                            $self->{tmpCheatLine}[0] = $element;
                            $self->{tmpCheatLine}[1] = $origtext;

                            # Enleve les blancs en debut de chaine
                            $self->{tmpCheatLine}[0] =~ s/^\s+//;
                            $self->{tmpCheatLine}[1] =~ s/^\s+//;
                            # Enleve les blancs en fin de chaine
                            $self->{tmpCheatLine}[0] =~ s/\s+$//;
                            $self->{tmpCheatLine}[1] =~ s/\s+$//;

                            push @{$self->{curInfo}->{code}}, $self->{tmpCheatLine} if ($self->{isCode});
                            push @{$self->{curInfo}->{unlockable}}, $self->{tmpCheatLine} if ($self->{isUnlock});
                            $self->{tmpCheatLine} = [];
                        }
                    }
                    $self->{tip_wait} = '';
                    $self->{isCode} = 0;
                    $self->{isUnlock} = 0;
                }
                else
                {
                    if ($self->{curInfo}->{secrets})
                    {
                        $self->{curInfo}->{secrets} .= "\n\n";
                    }
                    $self->{curInfo}->{secrets} .= $origtext;
                    $self->{curInfo}->{secrets} .= "\n\n" . $self->{tip_wait} if $self->{tip_wait} ne 1;
                    $self->{tip_wait} = '' if $self->{tip_wait} ne 1;
                }
            }
        }
        else
        {
            if ($self->{is})
            {
                $origtext =~ s/^\s*//;

                if ($self->{is} eq 'platform')
                {
                    $self->{curInfo}->{$self->{is}} = $origtext;
                    $self->{curInfo}->{platform} =~ s/DOS/PC/;
                    $self->{curInfo}->{platform} =~ s/Windows/PC/;
                }
                elsif ($self->{is} eq 'genre')
                {
                    push @{$self->{curInfo}->{genre}}, [ $origtext ];
                }
                elsif ($self->{is} eq 'released')
                {
                    # trying to normalize the date, choosing first day of month or year
                    $self->{curInfo}->{released} = GCUtils::strToTime((lc $origtext),"%b %e %Y", $self->getLang());
                    $self->{curInfo}->{released} = GCUtils::strToTime($self->{curInfo}->{released},"%b %Y", $self->getLang());
                    $self->{curInfo}->{released} = GCUtils::strToTime($self->{curInfo}->{released},"%Y", $self->getLang())
                       if (!($self->{curInfo}->{released} =~ m|/|));
                }
                else
                {
                    $self->{curInfo}->{$self->{is}} = $origtext;
                }

                $self->{is} = '';
            }
            elsif ($self->{isName})
            {
                $self->{curInfo}->{name} = $origtext;
                $self->{curInfo}->{platform} = $self->{url_plateforme};
                $self->{curInfo}->{exclusive} = 1;
                $self->{isName} = 0;
            }
            elsif ($self->{isRating})
            {
                $self->{curInfo}->{ratingpress} = int($origtext);
                $self->{isRating} = 0;
            }
            elsif ($self->{isDescription} eq 2)
            {
                    $self->{curInfo}->{description} .= $origtext." ";
            }
            elsif ($origtext eq 'Publishers')
            {
                $self->{isEditor} = 1;
            }
            elsif ($origtext eq 'Developers')
            {
                $self->{isDeveloper} = 1;
            }
            elsif ($origtext eq 'Platforms')
            {
                $self->{isExclusive} = 1;
            }
            elsif ( ($origtext eq 'Also For') || (($origtext eq 'Platforms')))
            {
                $self->{curInfo}->{exclusive} = 0;
            }
            elsif ($origtext eq 'Released')
            {
                $self->{isDate} = 1;
            }
            elsif ($origtext eq 'Genre')
            {
                $self->{isGenre} = 1;
            }
        }
    }

    sub getTipsUrl
    {
        my $self = shift;
        my $url = $self->{curInfo}->{$self->{urlField}}.'hints';
        $url =~ s/##MobyGames//;
        return $url;
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{hasField} = {
            name => 1,
            platform => 1,
            released => 1
        };

        $self->{isName} = 0;
        $self->{isPlatform} = 0;
        $self->{isAka} = 0;
        $self->{isEditor} = 0;
        $self->{isDeveloper} = 0;
        $self->{isDate} = 0;
        $self->{isGenre} = 0;
        $self->{isDescription} = 0;
        $self->{isSectionTips} = 0;
        $self->{isCode} = 0;
        $self->{isUnlock} = 0;
        $self->{is} = '';
        $self->{url_plateforme} = '';
        $self->{isExclusive} = 0;
        $self->{tip_wait} = '';
        $self->{type_wait} = '';
        $self->{isType_wait} = 0;

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{rootUrl} = $self->{loadedUrl};

        return $html;
    }

    sub normUrl
    {
        my ($self, $url) = @_;
        $url = 'http://www.mobygames.com'.$url
                        if ($url =~ m|^/|);
        return $url
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;
        return 'http://www.mobygames.com/search/?q='.$word.'&type=game';
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;

        my $found = index($url,"tpfplatformtpf");
        if ( $found >= 0 )
        {
            $self->{url_plateforme} = substr($url, $found +length('tpfplatformtpf'),length($url)- $found -length('tpfplatformtpf'));
            $url = substr($url, 0,$found);
        }

        return $url if $url;
        return 'http://www.mobygames.com/';
    }

    sub getName
    {
        return 'MobyGames';
    }

    sub getAuthor
    {
        return 'TPF';
    }

    sub getLang
    {
        return 'EN';
    }

    sub getCharset
    {
        my $self = shift;

        return "UTF-8";
    }
}

1;
