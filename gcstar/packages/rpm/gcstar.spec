Summary: GCstar, Collection manager
Name:    gcstar
Version: 1.8.0
Release: 1
Group:   Applications/Databases
License: GPLv2
Source:  GCstar-v%{version}.tar.gz

AutoReqProv: no
Requires:    perl
Requires:    perl-threads, perl-threads-shared
Requires:    perl-FindBin, perl-Unicode-Normalize
Requires:    perl(DateTime::Format::Strptime)
Requires:    perl(Gtk3)
Requires:    perl(Gtk3::SimpleList)
Requires:    perl(JSON)
Requires:    perl(LWP::Protocol::https)
Requires:    perl(XML::Simple)
Requires:    perl(Archive::Tar)
Requires:    perl(Archive::Zip)
Requires:    perl(Compress::Zlib)
Requires:    perl(Date::Calc)
Requires:    perl(Digest::MD5)
Requires:    perl(Time::Piece)
Requires:    perl(Locale::Codes)
Requires:    perl(MIME::Base64)
Recommends:  perl(GD)
Recommends:  perl(GD::Graph)
Recommends:  perl(GD::Text)
Recommends:  perl(Image::ExifTool)
Recommends:  perl(MP3::Info)
Recommends:  perl(MP3::Tag)
Recommends:  perl(Net::FreeDB)
Recommends:  perl(Ogg::Vorbis::Header::PurePerl)

BuildRoot: %{_tmppath}/%{name}-root
BuildArch: noarch
URL: https://www.gitlab.com/GCstar/GCstar

%description
GCstar - Application for managing your personal collections (books, films, games...).
Detailed information on items can be automatically retrieved
from the internet. New type of collections can be added. Lending to other people can be tracked.

%description -l fr
GCstar - Application permettant de gérer des collections personnelles (films, livres, jeux...).
Des recherches internet permettent de compléter les descriptions des éléments. Le prêt d'éléments est géré.
De nouveaux types de collections peuvent être ajoutés par les utilisateurs.

%prep
%setup -q -n %{name}

%build

%install
./install --prefix=${RPM_BUILD_ROOT}/usr/ --verbose --text --noclean --fhs

%files
%defattr(-,root,root)
/usr/share/gcstar/
/usr/share/applications/gcstar.desktop
/usr/share/icons/hicolor/*/apps/gcstar.png
/usr/share/icons/hicolor/*/mimetypes/application-x-gcstar.png
/usr/share/man/man1/gcstar.1.gz
/usr/share/mime/application/x-gcstar.xml
/usr/share/mime/packages/gcstar.xml
/usr/share/pixmaps/gcstar.png
%exclude    /usr/share/applications/mimeinfo.cache
%exclude    /usr/share/mime/XMLnamespaces
%exclude    /usr/share/mime/aliases
%exclude    /usr/share/mime/generic-icons
%exclude    /usr/share/mime/globs
%exclude    /usr/share/mime/globs2
%exclude    /usr/share/mime/icons
%exclude    /usr/share/mime/magic
%exclude    /usr/share/mime/mime.cache
%exclude    /usr/share/mime/subclasses
%exclude    /usr/share/mime/treemagic
%exclude    /usr/share/mime/types
%exclude    /usr/share/mime/version
%attr(0755,root,root) /usr/bin/gcstar

%changelog
* Sun Mar 10 2024 Kerenoc
  - follow FHS rules
* Sat Jun 10 2023 Kerenoc
  - RPM package for 1.8.0
* Mon Apr 03 2023 Kerenoc
  - Prepare 1.8.0 version
* Mon Dec 09 2019 Kerenoc
  - Fix build process
* Tue Feb 08 2005 Tian <tian@c-sait.net>
  - Initial spec file created by autospec ver. 0.8 with rpm 3 compatibility
