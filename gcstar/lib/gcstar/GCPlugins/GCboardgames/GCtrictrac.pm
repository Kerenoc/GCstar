package GCPlugins::GCboardgames::GCtrictrac;

###################################################
#
#  Copyright 2005-2016 Christian Jodar
#  Copyright 2017-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;

use GCPlugins::GCboardgames::GCboardgamesCommon;

{
    package GCPlugins::GCboardgames::GCPlugintrictrac;

    use base qw(GCPlugins::GCboardgames::GCboardgamesPluginsBase);
    use JSON qw( decode_json );

    sub parse
    {
        my ($self, $html) = @_;

        if ($self->{parsingList})
        {
            my $json = decode_json($html);

            for my $item (@{$json->{gameEdition}})
            {
                $self->{itemIdx}++;
                $self->{itemsList}[$self->{itemIdx}]->{url} = "https://trictrac.net/jeu-de-societe/".$item->{slug};
                $self->{itemsList}[$self->{itemIdx}]->{name} = $item->{title};
            }
        }
        else
        {
            HTML::Parser::parse($self,$html);
        }
    }

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        return if ($self->{parsingEnded});

        $self->{parsingEnded} = 1
            if ($tagname eq 'section' && $attr->{class} =~ /style_section/);

        if ($tagname eq "meta" && $attr->{itemprop})
        {
            if ($attr->{itemprop} eq 'suggestedMinAge')
            {
                $self->{curInfo}->{suggestedage} = $attr->{content};
            }
            elsif ($attr->{itemprop} eq 'minValue')
            {
                $self->{curInfo}->{players} = $attr->{content};
            }
            elsif ($attr->{itemprop} eq 'minValue')
            {
                $self->{curInfo}->{players} = $attr->{content};
            }
            elsif ($attr->{itemprop} eq 'ratingValue')
            {
                $self->{curInfo}->{ratingpress} = $attr->{content};
            }
        }
        elsif ($tagname eq "th")
        {
            $self->{isInfo} = 1;
        }
        elsif (($tagname eq "img") && ($attr->{itemprop} eq "image"))
        {
            $self->{curInfo}->{boxpic} = "https://trictrac.net".$attr->{src};
        }
        elsif ($tagname eq 'h1')
        {
            $self->{isTitle} = 1;
        }
        elsif ($tagname eq "div" && $attr->{title} && $attr->{title} eq 'Auteurs')
        {
            $self->{insideDesigner} = 1;
        }
        elsif ($tagname eq "div" && $attr->{title} && $attr->{title} eq 'Artistes')
        {
            $self->{insideIllustrator} = 1;
        }
        elsif ($tagname eq "div" && $attr->{title} && $attr->{title} eq 'Editeurs')
        {
            $self->{insidePublishers} = 1;
        }
        elsif ($tagname eq "summary")
        {
        	$self->{isInfo} = 0;
            $self->{insideDescription} = 2;
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

        return if ($self->{parsingEnded});

        if ($tagname eq 'span')
        {
            $self->{insideDesigner} = 2 if $self->{insideDesigner};
            $self->{insideIllustrator} = 2 if $self->{insideIllustrator};
            $self->{insidePublishers} = 2 if $self->{insidePublishers};
        }
        elsif (($tagname eq "div") && ($self->{isBoardgame} eq 1))
        {
            $self->{isBoardgame} = 0;
        }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        return if ($self->{parsingEnded});

        return if (length($origtext) < 2);

        $origtext =~ s/&#34;/"/g;
        $origtext =~ s/&#179;/3/g;
        $origtext =~ s/\n//g;
        $origtext =~ s/^\s\{2,//;
        #French accents substitution
        #$origtext =~ s/&agrave;/é/;
        #$origtext =~ s/&eacute;/è/;

        # Enleve les blancs en debut de chaine
        $origtext =~ s/^\s+//;
        # Enleve les blancs en fin de chaine
        $origtext =~ s/\s+$//;

        return if ($origtext eq '');

        if ($self->{isTitle} eq 1)
        {
            $self->{curInfo}->{name} = $origtext;
            $self->{isTitle} = 0;
        }
        # fetching information from page
        elsif ($self->{insideDesigner} eq 2)
        {
            $self->{curInfo}->{designedby} = $origtext;
            $self->{insideDesigner} = 0;
        }
        elsif ($self->{insideIllustrator} eq 2)
        {
            $self->{curInfo}->{illustratedby} = $origtext;
            $self->{insideIllustrator} = 0;
        }
        elsif ($self->{insidePublishers} eq 2)
        {
            $self->{curInfo}->{publishedby} = $origtext;
            $self->{insidePublishers} = 0;
        }
        elsif ($self->{insidePlayingTime})
        {
            $origtext =~ s/ *min.*//;
            $self->{curInfo}->{playingtime} = $origtext if $origtext ne "null";
            $self->{insidePlayingTime} = 0
        }
        elsif (($self->{isInfo} eq 1 ) && !($origtext eq "et") && !($origtext eq ","))
        {
            if ($origtext =~ /Auteurs/)
            {
                $self->{insideDesigner} = 1;
            }
            elsif ($origtext =~ /Illustrateurs/)
            {
                $self->{insideIllustrator} = 1;
            }
            elsif ($origtext =~ /diteurs/)
            {
                $self->{insidePublishers} = 1;
            }
            elsif ($origtext =~ m/e de partie/)
            {
                $self->{insidePlayingTime} = 1;
            }
        }
        elsif ($self->{insideDescription} eq 2)
        {
            $self->{curInfo}->{description} .= $origtext." ";
        }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{hasField} = {
            name => 1,
            released => 1,
        };

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{parsingEnded} = 0;

        foreach my $item( ( 'Description', 'Designer', 'Illustrator',
                            'PlayingTime', 'Pictures', 'Publishers' ) )
        {
            $self->{"inside$item"} = 0;
        }

        return $html;
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        return "https://trictrac.net/api/search?queryString=".$word."&categories=game,gameEdition";
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;

        return $url if (($url =~ /^http:/) || ($url =~ /^https:/));
        if ($url =~ /^\//)
        {
            return "http://trictrac.net".$url;
        }
        else
        {
            return "http://trictrac.net/".$url;
        }
    }

    sub getName
    {
        return "Tric Trac";
    }

    sub getAuthor
    {
        return 'Florent - Kerenoc';
    }

    sub getLang
    {
        return 'FR';
    }

    sub getDefaultPictureSuffix
    {
        return '.webp';
    }
}

1;
