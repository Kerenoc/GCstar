package GCPlugins::GCfilms::GCAniDB;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCfilms::GCfilmsCommon;

{
    package GCPlugins::GCfilms::GCPluginAniDB;

    use base qw(GCPlugins::GCfilms::GCfilmsPluginsBase);

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        $self->{inside}->{$tagname}++;

        return if ($self->{parsingEnded});

        if ($self->{parsingList})
        {
            if ($tagname eq 'a')
            {
                if ($attr->{href} =~ m/\/anime\/[0-9]+/)
                {
                    $self->{isMovie} = 1;
                    $self->{isInfo} = 1;
                    $self->{itemIdx}++ if ($self->{itemIdx} < 0) || ($attr->{href} ne $self->{itemsList}[$self->{itemIdx}]->{url});
                    $self->{itemsList}[$self->{itemIdx}]->{url} = $attr->{href};
                }
            }
            elsif ($tagname eq 'td')
            {
                $self->{isYear} = 1 if ($attr->{class} eq 'date year');
            }
            elsif ($tagname eq 'h1')
            {
                $self->{insideHeadline} = 1;
            }
        }
        else
        {
            if ($tagname eq 'img')
            {
                if ($attr->{src} =~ m/\/images\/main\//)
                {
                    $self->{curInfo}->{image} = $attr->{src} if !$self->{curInfo}->{image};
                }
            }
            elsif ($tagname eq 'div')
            {
                if ($attr->{itemprop} && $attr->{itemprop} eq 'description')
                {
                    $self->{insideSynopsis} = 1;
                }
                elsif ($attr->{id} && $attr->{id} eq 'userstats')
                {
                    $self->{insideSynopsis} = 0;
                }
            }
            elsif ($tagname eq 'br/' && $self->{insideSynopsis})
            {
                $self->{curInfo}->{synopsis} .= "\n";
            }
            elsif ($tagname eq 'th')
            {
                $self->{isField} = 1 if $attr->{class} eq 'field';
            }
            elsif ($tagname eq 'label' && $attr->{itemprop} eq 'alternateName' && $self->{isOriginal})
            {
                $self->{isOriginal} = 2;
            }
            elsif ($tagname eq 'span' && $attr->{itemprop} && $attr->{itemprop} eq 'genre' && $self->{isGenre})
            {
                $self->{isGenre} = 2;
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

        $self->{inside}->{$tagname}--;;
    }

    sub text
    {
        my ($self, $origtext) = @_;

        return if ($self->{parsingEnded});

        $origtext =~ s/[\r\n]//g;
        $origtext =~ s/^\s*//;
        $origtext =~ s/\s*$//;

        return if ($origtext eq '');

        if ($self->{parsingList})
        {
            if ($self->{insideHeadline})
            {
                $self->{parsingEnded} = 1 if $origtext !~ m/Anime List - Results for:/;
                $self->{insideHeadline} = 0;
            }

            if ($self->{isMovie})
            {
                $self->{itemsList}[$self->{itemIdx}]->{title} = $origtext
                    if ! $self->{itemsList}[$self->{itemIdx}]->{title};
                $self->{isMovie} = 0;
                $self->{isInfo} = 1;
                return;
            }
            elsif ($self->{isYear})
            {
                $self->{itemsList}[$self->{itemIdx}]->{date} = $origtext;# if $origtext =~ m/^ [0-9]{4}(-[0-9]{4})? $/;
                $self->{isYear} = 0;
            }
        }
        else
        {
            if ($self->{insideSynopsis})
            {
                $origtext =~ s/\s{2,}/ /g;
                $self->{curInfo}->{synopsis} .= $origtext;
            }
            elsif ($self->{isField})
            {
                $self->{isOriginal} = 0;
                $self->{isGenre} = 0;
                $self->{isTitle} = 1 if $origtext eq 'Main Title';
                $self->{isOriginal} = 1 if $origtext eq 'Official Title';
                $self->{isOrig} = 1 if $origtext =~ /kanji/i;
                $self->{isYear} = 1 if $origtext eq 'Year';
                $self->{isGenre} = 1 if $origtext eq 'Tags';
                $self->{isRating} = 1 if $origtext eq 'Rating';
                $self->{isField} = 0;
            }
            elsif ($self->{inside}->{td})
            {
                if ($self->{isTitle})
                {
                    $self->{curInfo}->{title} = $origtext;
                    $self->{isTitle} = 0;
                }
                elsif ($self->{isOriginal} eq 2)
                {
                    return if grep(/$origtext/, $self->{curInfo}->{original});
                    $self->{curInfo}->{original} .= " - " if $self->{curInfo}->{original};
                    $self->{curInfo}->{original} .= $origtext;
                    $self->{isOriginal} = 1;
                }
                elsif ($self->{isYear})
                {
                    if ($origtext =~ /([0-9][0-9])\.([0-9][0-9])\.([0-9][0-9][0-9][0-9])/)
                    {
                        $self->{curInfo}->{date} = "$1/$2/$3";
                    }
                    else
                    {
                        $self->{curInfo}->{date} = $origtext;
                    }
                    $self->{isYear} = 0;
                }
                elsif ($self->{isGenre} eq 2)
                {
                    $self->{curInfo}->{genre} .= ", " if $self->{curInfo}->{genre};
                    $self->{curInfo}->{genre} .= $origtext;
                    $self->{isGenre} = 1;
                }
                elsif ($self->{isRating})
                {
                    ($self->{curInfo}->{ratingpress} = $origtext) =~ s/\s//g;
                    $self->{isRating} = 0;
                }
            }
        }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{hasField} = {
            title => 1,
            date => 1,
            director => 0,
            actors => 0,
        };

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{parsingEnded} = 0;
        $self->{isField} = 0;
        $self->{isInfo} = 0;
        $self->{isMovie} = 0;
        $self->{isGenre} = 0;
        $self->{isTitle} = 0;
        $self->{isOriginal} = 0;
        $self->{isRating} = 0;
        $self->{curName} = undef;
        $self->{curUrl} = undef;

        return $html;
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        return "http://anidb.net/perl-bin/animedb.pl?show=animelist&adb.search=$word";
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;

        return 'http://anidb.net' . $url;
    }

    sub getName
    {
        return 'AniDB';
    }

    sub getAuthor
    {
        return 'MeV';
    }

    sub getLang
    {
        return 'EN';
    }

    sub getCharset
    {
        my $self = shift;

        return "UTF-8";
    }
}

1;
