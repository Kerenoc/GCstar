package GCPlugins::GCbooks::GCBdphile;

###################################################
#
#  Copyright 2005-2007 Jonas
#  Copyright 2016-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCbooks::GCbooksCommon;

{
    package GCPlugins::GCbooks::GCPluginBdphile;

    use base 'GCPlugins::GCbooks::GCbooksPluginsBase';

    use URI::Escape;

    use GCUtils;

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        $word = uri_unescape($word);
        $word = uri_escape_utf8($word);
        $word =~ s/%2B/+/g;

        $self->{searchWord} = $word;

        if ($self->{searchField} eq $self->{seriesField})
        {
            return 'https://www.bdphile.info/search/series/?q='.$word
        }
        elsif ($self->{searchField} eq 'title' || $self->{searchField} eq 'isbn')
        {
            return 'https://www.bdphile.info/search/album/?q='.$word;
        }
        else
        {
            return 'https://www.bdphile.info/search/author/?q='.$word;
        }
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;

        return $url;
    }

    sub getCharset
    {
        my $self = shift;

        return "ISO-8859-1";
    }

    sub getName
    {
        return "Bdphile";
    }

    sub getAuthor
    {
        return 'Jonas - Kerenoc';
    }

    sub getLang
    {
        return 'FR';
    }

    sub getSearchFieldsArray
    {
        my $self = shift;

        return ['title', 'serie', 'authors', 'isbn', ];
    }

    # getNumberPasses
    # tests:
    #    title:Pawnee => single one shot
    #    title:Aquablue => multiple albums
    #    title:Tintin => too many albums
    #    serie:La Belette => multiple edition of one-shot
    #    serie:Blanco => single serie
    #    writer:Pellicer => single author/writer then only one-shots
    #    writer:Lidwine => single author/writer then mix of one-shots and series
    #    writer:Uderzo => multiple authors

    sub getNumberPasses
    {
        my $self = shift;

        # two pass to choose a serie then an album
        return 2 if ($self->{searchField} eq $self->{seriesField});
        # one pass when searching an album
        # unless they are too many results and another pass is needed to select a serie
        return $self->{pass} if ($self->{searchField} eq 'title' && $self->{lastPass});
        # three pass to choose writer then serie then album
        return 3 if ($self->{searchField} eq $self->{authorField});
        return 2;
    }

    sub getReturnedFields
    {
        my $self = shift;

        if ($self->{pass} == 1 && $self->{searchField} eq $self->{seriesField})
        {
            $self->{hasField} = {
                $self->{seriesField} => 1,
            };
        }
        elsif ($self->{pass} == 1 && $self->{searchField} eq $self->{authorField})
        {
            $self->{hasField} = {
                $self->{authorField} => 1,
            };
        }
        else
        {
            $self->{hasField} = {
                $self->{seriesField} => 1,
                title  => 1,
                volume => 1,
                $self->{publicationField} => 1,
            };
        }
    }

    sub changeUrl
    {
        my ($self, $url) = @_;

        return $url;
    }

    sub addSearchItem
    {
        my ($self, $attr) = @_;

        return if ($self->{searchField} ne $self->{authorField} && $attr->{href} =~ m|author|);
        $self->{itemIdx}++ ;
        $self->{isPublication} = 0 ;
        $attr->{href} =~ s|author/view|author/series|;
        $self->{itemsList}[$self->{itemIdx}]->{url} = $attr->{href} ;
        $self->{itemsList}[$self->{itemIdx}]->{nextUrl} = $attr->{href} ;
        $self->{itemsList}[$self->{itemIdx}]->{$self->{seriesField}} = $self->{serie};
        my $title = $self->{titleAlbum};
        $title = $attr->{title} if ($attr->{title});
        if ($title)
        {
            if ($title =~ /^(\d+|INT|COF)\. (.*)/)
            {
                $self->{itemsList}[$self->{itemIdx}]->{title} = $2;
                $self->{itemsList}[$self->{itemIdx}]->{volume} = $1;
            }
            else
            {
                $self->{itemsList}[$self->{itemIdx}]->{title} = $title;
            }
        }
        else
        {
            $self->{isTitle} = 1 ;
        }
    }

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;
        $self->{inside}->{$tagname}++;

        $self->{stopSearch} = 1 if ($tagname eq 'footer');
        return if ($self->{stopSearch});
        if (!$self->{started})
        {
            if ($tagname eq 'section' && $attr->{class} =~ m/details-section/)
            {
                $self->{started} = 1;
                $self->{type} = 'album';
            }
            elsif (($tagname eq 'section' && $attr->{id} =~ m/page-title/)
                    || ($tagname eq 'div' && $attr->{id} && $attr->{id} eq 'list_view'))
            {
                $self->{started} = 1;
                $self->{type} = 'serie';
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/wrapper padding/)
            {
                $self->{started} = 1;
                $self->{type} = 'search';
            }
        }
        return if ($self->{started} ne 1 || $self->{ended});

        if ($self->{parsingList})
        {
            if ($self->{loadedUrl} =~ m|/album/| && ! ($self->{loadedUrl} =~ m|info/search|))
            {
                # album : end of search
                $self->{itemIdx} = 0;
                $self->{itemsList}[0]->{url} = $self->{loadedUrl};
            }
            elsif ($tagname eq 'link' && $attr->{rel} eq 'canonical' && $attr->{href} =~ m|/album/|)
            {
                # album after redirection 302 from serie
                # exemple: author=Pellicer => serie=Le Charles de Gaulle)
                $self->{url} = $attr->{href};
            }
            elsif ($tagname eq 'a')
            {
                if (    ($self->{pass} eq 1 && ( $attr->{href} =~ m|/series/view| || $attr->{href} =~ m|/author/view| ))
                     || ($attr->{href} =~ m;/(series|album)/(bd|manga|comics)/; )
                     || ($attr->{href} =~ m;/album/view/; ) )
                {
                    return if ($attr->{href} =~ m/series/ &&
                                (($self->{pass} > 2 && $self->{searchField} eq $self->{authorField})
                                 || ($self->{pass} > 1 && $self->{searchField} ne $self->{authorField})));
                    (my $serie = $attr->{href}) =~ s/\/(bd|manga|comics)\/(\d+)\-?.*/\/view\/$2/;
                    return if $self->{loadedUrl} eq $serie;
                    $attr->{href} =~ s|/$||;
                    $attr->{href} =~ s|\#.*||;
                    return if $self->existsUrl($attr->{href});
                    $self->addSearchItem($attr);
                    $self->{lastPass} = 1 if ($attr->{href} =~ m;(info/album|album/view););
                }
            }
            elsif ($tagname eq 'h1' || $tagname eq 'h3')
            {
                $self->{isSerie} = 1;
            }
            elsif ($tagname eq 'h2' && $self->{serie} && ! $self->{titleAlbum})
            {
                $self->{isTitle} = 1;
            }
            elsif ($tagname eq 'h4' && $attr->{class} && $attr->{class} eq 'h-album')
            {
                $self->{isTitle} = 1;
            }
            elsif ($tagname eq 'h4' && $attr->{class} && $attr->{class} eq 'h-series')
            {
                $self->{isSerie} = 1;
            }
            elsif ($tagname eq 'img' && $self->{isTitle})
            {
                $self->{itemsList}[$self->{itemIdx}]->{title} = $attr->{alt};
                $self->{isTitle} = 0;
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} eq 'hidden')
            {
                $self->{isHiddenDiv} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{id} && $attr->{id} eq 'list_view')
            {
                $self->{isReeditions} = 1;
            }
            elsif (($tagname eq 'table' && $attr->{class} && $attr->{class} eq 'listing') && $self->{isHiddenDiv} == 0)
            {
                $self->{isReeditions} = 1;
            }
        }
        else
        {
            if (($tagname eq 'div') && $attr->{id} && ($attr->{id} eq "reeditions"))
            {
                $self->{isReeditions} = 1;
            }
            elsif ($self->{isReeditions} == 0)
            {
                if ($tagname eq 'h1' && ! $self->{curInfo}->{$self->{seriesField}})
                {
                    $self->{isSerie} = 1 ;
                }
                elsif ($tagname eq 'h2' && ! $self->{isTitle})
                {
                    $self->{isTitle} = 1 ;
                }
                elsif (    $tagname eq 'div' && $attr->{id} && $attr->{id} eq 'album'
                        && $self->{curInfo}->{title} ne $self->{newTitle}
                        && $self->{curInfo}->{$self->{seriesField}} )
                {
                    $self->{curInfo}->{title} = $self->{curInfo}->{$self->{seriesField}};
                }
                elsif ($tagname eq 'dt')
                {
                    $self->{isFieldName} = 1;
                    $self->{fieldName} = "" ;
                }
                elsif ($tagname eq 'dd')
                {
                    $self->{isFieldValue} = 1;
                    $self->{searchingTitle} = 0;
                }
                elsif (($tagname eq 'p') && ($attr->{class} =~ /^synopsis .*/))
                {
                    $self->{isDescription} = 1;
                }
                elsif (($tagname eq 'div') && $attr->{id} && ($attr->{id} eq "book-picture"))
                {
                    $self->{isCover} = 1;
                }
                elsif ($self->{isCover} && ($tagname eq 'a'))
                {
                    $self->{curInfo}->{$self->{coverField}} = $attr->{href};
                    $self->{isCover} = 0 ;
                    $self->{isBackpic} = 1 ;
                }
                elsif ($self->{isBackpic} && ($tagname eq 'a') && ($attr->{href} =~ m/image/))
                {
                    $self->{curInfo}->{backpic} = "https://www.bdphile.info/static/".$attr->{href}
                        if (! ($attr->{href} =~ m/http/));
                    $self->{isBackpic} = 0 ;
                }
                elsif ($tagname eq 'span' && $attr->{class} && $attr->{class} =~ m/language/)
                {
                    $self->{isSerie} = 2;
                }
                elsif ($tagname eq 'span' && $attr->{id} && $attr->{id} =~ m/rate/)
                {
                    $self->{isRating} = 1;
                }
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;
        $self->{inside}->{$tagname}--;

        if ($self->{parsingList})
        {
            if ($self->{isTitle} && $tagname eq 'a')
            {
                $self->{isTitle} = 0 ;
            }
            elsif ($tagname eq 'script' && $self->{isReeditions})
            {
                $self->{stopSearch} = 1;
                $self->{isReeditions} = 0;
            }
            elsif ($tagname eq 'div' && $self->{isHiddenDiv})
            {
                $self->{isHiddenDiv} = 0;
            }
            elsif ($tagname eq 'table')
            {
                $self->{ended} = 1;
            }
        }
        else
        {
            if ($self->{isReeditions} == 0)
            {
                if ($self->{isSerie} && ($tagname eq 'h1' || $tagname eq 'a'))
                {
                    $self->{isSerie} = 0;
                }
                elsif ($tagname eq 'h2' && $self->{isTitle})
                {
                    $self->{isTitle} = 0;
                }
                elsif ($tagname eq 'dt')
                {
                    $self->{isFieldName} = 0;
                }
                elsif ($tagname eq 'dd')
                {
                    if ($self->{author} ne "")
                    {
                        if ($self->{searchType} eq 'books')
                        {
                            if (! (grep /$self->{author}/, @{$self->{curInfo}{authors}}))
                            {
                                push @{$self->{curInfo}{authors}}, [$self->{author}];
                            }
                        }
                        else
                        {
                            $self->{curInfo}->{writer} .= ', ' if $self->{curInfo}->{writer};
                            $self->{curInfo}->{writer} .= $self->{author};
                        }
                        $self->{author} = "";
                    }
                    $self->{isFieldValue} = 0
                }
                elsif ($tagname eq 'div')
                {
                    $self->{isDescription} = 0;
                }
            }
        }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        # Enleve les blancs en debut et fin de chaine
        $origtext =~ s/^[\s\n]+//;
        $origtext =~ s/[\s\t]+$//;
        return if $origtext eq '';

        if ($self->{parsingList})
        {
            if ($self->{isTitle})
            {
                print " ".$origtext;
                if ($origtext =~ m/La série/)
                {
                    $self->{itemIdx}--;
                }
                elsif ($origtext eq 'Bibliographie' && $self->{searchField} eq $self->{authorField})
                {
                    # example : author=Pellicer
                    $self->{isTitle} = 0;
                }
                elsif ($origtext =~ /(.*)\s(\d+)\.\s(.*)/)
                {
                    $self->{itemsList}[$self->{itemIdx}]->{title} = $3;
                    $self->{itemsList}[$self->{itemIdx}]->{$self->{seriesField}} = $1;
                    $self->{itemsList}[$self->{itemIdx}]->{volume} = $2;
                }
                elsif ($origtext =~ /(\d+)\.\s(.*)/)
                {
                    $self->{itemsList}[$self->{itemIdx}]->{title} = $2;
                    $self->{itemsList}[$self->{itemIdx}]->{volume} = $1;
                }
                else
                {
                    $self->{itemsList}[$self->{itemIdx}]->{$self->{authorField}} = $origtext
                       if $self->{itemsList}[$self->{itemIdx}]->{url} =~ m,/author/,;
                    $self->{itemsList}[$self->{itemIdx}]->{title} = $origtext
                       if $self->{itemsList}[$self->{itemIdx}]->{url} =~ m,/album/,;
                    $self->{itemsList}[$self->{itemIdx}]->{$self->{seriesField}} = $1
                        if $origtext =~ /(.*)\s(INT|COF)\.\s/;
                    $self->{itemsList}[$self->{itemIdx}]->{$self->{seriesField}} = $origtext
                       if $self->{itemsList}[$self->{itemIdx}]->{url} =~ m,/series/,;
                }
                $self->{itemsList}[$self->{itemIdx}]->{$self->{seriesField}} = $self->{serie}
                    if ($self->{itemIdx} ne -1 && $self->{serie}
                        && ! $self->{itemsList}[$self->{itemIdx}]->{$self->{seriesField}} );
                $self->{isTitle} = 0;
                $self->{isPublication} = 1 if $self->{type} eq 'search' && $self->{searchField} eq 'title';
            }
            elsif ($self->{isSerie})
            {
                if ($origtext =~ m/(Vous ne trouvez pas|Séries proches)/)
                {
                    $self->{stopSearch} = 1;
                }
                elsif (!($origtext =~ m/votre recherche/) && ! $self->{serie})
                {
                    $self->{serie} = $origtext;
                }
                else
                {
                    if ($origtext eq '(one-shot)')
                    {
                        $self->{titleAlbum} = $self->{serie};
                        $self->{$self->{seriesField}} = 'One-shot';
                    }
                    $self->{isSerie} = 0;
                }
            }
            elsif ($self->{isPublication})
            {
                return if ($self->{itemIdx} < 0);
                $origtext =~ s/^- *//;
                return if ! $origtext;
                if ($origtext =~ /(\d+)\.(.*)/)
                {
                    $self->{itemsList}[$self->{itemIdx}]->{title} = $2;
                    $self->{itemsList}[$self->{itemIdx}]->{volume} = $1;
                }
                $self->{itemsList}[$self->{itemIdx}]->{$self->{publicationField}} = $origtext;
                $self->{isPublication} = 0;
            }
            elsif ($origtext =~ m/affiner pour afficher/i || $origtext =~ m/Aucun r.*sultat/i )
            {
                # too many results, search for series instead of albums
                $self->{nextUrl} = 'https://www.bdphile.info/search/series/?q='.$self->{searchWord};
            }
            elsif ($self->{isReeditions} && $origtext =~ m/Séries proches/)
            {
                $self->{isReeditions} = 0;
                $self->{stopSearch} = 1;
            }
            elsif ($origtext =~ m/Publié (le|en)/)
            {
                $self->{isPublication} = 1;
            }
        }
        else
        {
            if ($self->{isReeditions} == 0)
            {
                if ($self->{isSerie} eq 1)
                {
                    $self->{curInfo}->{$self->{seriesField}} .= $origtext." " if ($origtext !~ m/\(\s*one-shot\s*\)/);
                }
                elsif ($self->{isTitle} && ! $self->{curInfo}->{title})
                {
                    if ($origtext =~ m/^(Synopsis|Détails|BD)$/)
                    {
                        # one-shot
                        $self->{isTitle} = 0;
                        $self->{curInfo}->{title} = $self->{curInfo}->{$self->{seriesField}};
                        $self->{curInfo}->{$self->{seriesField}} = 'One-shot' if ($self->{searchType} eq 'books');
                        $self->{curInfo}->{title} = "One-shot" if ($self->{searchType} eq 'comics');
                        $self->{curInfo}->{volume} = 0;
                        $self->{curInfo}->{$self->{descriptionField}} = "One-shot\n\n";
                    }
                    else
                    {
                        $self->{curInfo}->{title} = $origtext;
                    }
                    if ($self->{curInfo}->{$self->{seriesField}} ne "")
                    {
                        $self->{curInfo}->{volume} = $1 if ($origtext =~ /Tome *([\d]*)/i);
                        $self->{curInfo}->{title} = $2 if ($origtext =~ /Tome *([\d]*) *: *(.*)/i);
                        if ($origtext =~ /^([\d]+)\. *(.*)/i)
                        {
                            $self->{curInfo}->{volume} = $1;
                            $self->{curInfo}->{title} = $2;
                        }
                    }
                    $self->{newTitle} = $self->{curInfo}->{title};
                }
                elsif ($self->{isFieldName})
                {
                    $self->{fieldName} = $origtext;
                }
                elsif ($self->{isFieldValue})
                {
                    if (($self->{fieldName} =~ m/Sc.*nario/) || $self->{fieldName} =~ /Auteur/  || $self->{fieldName} =~ /Textes/)
                    {
                        $self->{author} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "Dessin")
                    {
                        if ($self->{searchType} eq 'books')
                        {
                            if (! (grep /$origtext/, @{$self->{curInfo}{authors}}))
                            {
                                push @{$self->{curInfo}{authors}}, [$origtext];
                            }
                        }
                        $self->{curInfo}->{$self->{illustratorField}} .= ', ' if $self->{curInfo}->{$self->{illustratorField}};
                        $self->{curInfo}->{$self->{illustratorField}} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "Lettrage")
                    {
                        $self->{curInfo}->{$self->{lettererField}} .= ', ' if $self->{curInfo}->{$self->{lettererField}};
                        $self->{curInfo}->{$self->{lettererField}} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "Couleurs")
                    {
                        $self->{curInfo}->{$self->{colouristField}} .= ', ' if $self->{curInfo}->{$self->{colouristField}};
                        $self->{curInfo}->{$self->{colouristField}} .= $origtext;
                    }
                    elsif ($self->{fieldName} =~ /diteur/) # Éditeur (potential pb with accent)
                    {
                        $self->{curInfo}->{publisher} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "Date de publication")
                    {
                        $origtext = GCUtils::strToTime($origtext,"%d %B %Y",$self->getLang());
                        $origtext = GCUtils::strToTime($origtext,"%d %b %Y",$self->getLang());
                        $origtext = GCUtils::strToTime($origtext,"%B %Y",$self->getLang());
                        $origtext = GCUtils::strToTime($origtext,"%Y",$self->getLang()) if ($origtext =~ /^\d+$/);
                        $self->{curInfo}->{$self->{publicationField}} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "Collection" && $self->{searchType} ne 'books')
                    {
                        $self->{curInfo}->{collection} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "Format")
                    {
                        $self->{curInfo}->{format} .= $origtext;
                        if ($origtext =~ /([\D]+) - ([\d]*) *pages - ([\d]*\.*[\d]*)€/i)
                        {
                            $self->{curInfo}->{format} = $1;
                            $self->{curInfo}->{$self->{nbpagesField}} = $2;
                            $self->{curInfo}->{cost} = $3;
                        }
                        elsif ($origtext =~ /([\D]+) - ([\d]*) *pages/i)
                        {
                            $self->{curInfo}->{format} = $1;
                            $self->{curInfo}->{$self->{nbpagesField}} = $2;
                        }
                        if ($origtext =~ /([\d]*) *pages - ([\d]*\.*[\d])€/i)
                        {
                            $self->{curInfo}->{$self->{nbpagesField}} = $1;
                            $self->{curInfo}->{cost} = $2;
                        }
                        elsif ($origtext =~ /([\d]*) *pages/i)
                        {
                            $self->{curInfo}->{$self->{nbpagesField}} = $1;
                        }
                    }
                    elsif ($self->{fieldName} eq "EAN")
                    {
                        $origtext =~ s/\-//g;
                        $self->{curInfo}->{isbn} = $origtext;
                    }
                }
                elsif ($self->{isRating})
                {
                    $self->{curInfo}->{ratingpress} = 2 * $origtext if $origtext =~ m/[0-9,]+/;
                    $self->{isRating} = 0;
                }
                elsif (($self->{isDescription}) && ($origtext ne "Synopsis"))
                {
                    $self->{curInfo}->{$self->{descriptionField}} .= $origtext."\n";
                }
            }
        }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();

        bless ($self, $class);

        $self->initParams();

        return $self;
    }

    sub initParams
    {
        my $self = shift;

        $self->{hasField} = {
            serie => 1,
            title => 1,
            authors => 0,
            artist => 0,
            publisher => 0,
            publication => 1,
            format => 0,
            pages => 0,
            edition => 0,
            web => 0,
            description => 0,
            isbn => 0,
        };

        $self->{lastPass} = 0;

        $self->{searchField} = ($self->getSearchFieldsArray())[0][0]
             if (! $self->{searchField});

        @{$self->{curInfo}{authors}} = ();
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{isTitle} = 0;
        $self->{isHiddenDiv} = 0;
        $self->{isReeditions} = 0;
        $self->{isPublication} = 0;
        $self->{author} = "";
        $self->{stopSearch} = 0;
        $self->{serie} = "";
        $self->{titleAlbum} = "";
        $self->{started} = 0;
        $self->{ended} = 0;

        $self->{lastPass} = 0 if ($self->{parsingList});

        return $html;
    }
}

1;
