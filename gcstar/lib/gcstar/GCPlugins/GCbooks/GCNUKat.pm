package GCPlugins::GCbooks::GCbooksNUKat;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2017-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCbooks::GCbooksCommon;

my $searchURL = "";

{
    package GCPlugins::GCbooks::GCPluginNUKat;

    use base qw(GCPlugins::GCbooks::GCbooksPluginsBase);

    use URI::Escape;

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        $self->{inside}->{$tagname}++;

        if ($self->{parsingList})
        {
            if ($tagname eq 'img' && $attr->{alt} && $attr->{alt} eq 'books')
            {
                $self->{isBook} = 1;
            }
            elsif ($self->{isBook} && $tagname eq 'a' && $attr->{class} && $attr->{class} eq 'title')
            {

                $self->{itemIdx}++;
                $attr->{href} =~ s/\.\.\///;
                $self->{itemsList}[$self->{itemIdx}]->{url} = "http://katalog.nukat.edu.pl/".$attr->{href};
                $self->{isTitle} = 1;
                $self->{isBook} = 0;
            }
            elsif ($tagname eq 'a' && $attr->{class} && $attr->{class} eq 'item')
            {
                $self->{nextPage} = $attr->{href}
                  if($attr->{href} =~ m/pageNumber=/);
            }
            elsif ($tagname eq 'i' && $attr->{class} && $attr->{class} =~ m/chevron-right/)
            {
                $self->{nextPage} =~ s/\.\///;
                $self->{nextUrl} = "http://katalog.nukat.edu.pl/search/".$self->{nextPage}
                  if $self->{nextPage};
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/recordfields/)
            {
                $self->{isDetails} = 1 if ($self->{itemIdx} > -1);
            }
        }
        else
        {
            if ($tagname eq 'h2' && $attr->{id} && $attr->{id} eq 'title')
            {
                $self->{isTitle} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} eq 'row recordfields')
            {
                $self->{isDetail} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/recordImage/)
            {
                $self->{isCover} = 1;
                $self->{curInfo}->{isbn} = $attr->{'data-isbn'} if $attr->{'data-isbn'};
            }
            elsif ($self->{isCover} && $tagname eq 'img')
            {
                $self->{curInfo}->{cover} = $attr->{src};
                $self->{isCover} = 0;
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

        $self->{inside}->{$tagname}--;
        if ($tagname eq 'div')
        {
            $self->{isDetails} = 0 ;
        }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        $origtext =~ s/^[\s\n]+//;
        $origtext =~ s/[\s\t]+$//;
        return if $origtext eq '';

        if ($self->{parsingList})
        {
            if ($self->{isTitle})
            {
                ($self->{itemsList}[$self->{itemIdx}]->{title} = $origtext) =~ s/ *\/.*//;
                $self->{itemsList}[$self->{itemIdx}]->{authors} = $1
                    if ($origtext =~ m/\/(.*)/);
                $self->{isTitle} = 0;
            }
            elsif ($self->{isDetails} && $origtext eq 'Adres wydawniczy')
            {
                $self->{isPublisher} = 1;
                $self->{isDetails} = 0;
            }
            elsif ($self->{isPublisher})
            {
                $self->{itemsList}[$self->{itemIdx}]->{edition} = $origtext;
                $self->{isPublisher} = 0;
            }
        }
        else
        {
            if ($self->{isTitle})
            {
                ($self->{curInfo}->{title} = $origtext) =~ s/\/.*//;
                $self->{isTitle} = 0;
            }
            elsif ($self->{isAuthor})
            {
                ($self->{curInfo}->{authors} = $origtext) =~ s/\(.*//;
                $self->{isAuthor} = 0;
            }
            elsif ($self->{isDetail})
            {
                $self->{isAuthor}    = 1 if $origtext eq 'Autor';
                $self->{isISBN}      = 1 if $origtext eq 'ISBN';
                $self->{isFormat}    = 1 if $origtext eq 'Opis fizyczny';
                $self->{isPublisher} = 1 if $origtext eq 'Adres wydawniczy';
                $self->{isOriginal}  = 1 if $origtext eq 'Tytuł ujednolicony';
                $self->{isGenre}     = 1 if $origtext eq 'Gatunek';
                $self->{isDetail} = 0;
            }
            elsif ($self->{isFormat})
            {
                ($self->{curInfo}->{format} = $origtext) =~ s/.*; //;
                $self->{curInfo}->{pages} = $1
                    if ($origtext =~ m/(\d+) s\./);
                $self->{isFormat} = 0;
            }
            elsif ($self->{isISBN})
            {
                $self->{curInfo}->{isbn} = $origtext if (! $self->{curInfo}->{isbn});
                $self->{isISBN} = 0;
            }
            elsif ($self->{isPublisher})
            {
                $origtext =~ s/cop\.\s?//;
                $origtext =~ s/Published by\s?//;
                $self->{curInfo}->{publisher} = $origtext;
                if ($origtext =~ m/.*: *(.*), *(\d+).?/)
                {
                    $self->{curInfo}->{publisher} = $1;
                    $self->{curInfo}->{publication} = $2;
                }
                $self->{isPublisher} = 0;
            }
            elsif ($self->{isOriginal})
            {
                $self->{curInfo}->{original} = $origtext;
                $self->{isOriginal} = 0;
            }
         }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{hasField} = {
            title => 1,
            authors => 1,
            publication => 0,
            format => 0,
            edition => 1,
        };

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{parsingEnded} = 0;
        $self->{insideResults} = 0;
        $self->{actorsCounter} = 0;

        foreach my $field( ( 'Book', 'Title','Detail', 'Author', 'ISBN','Format', 'Publisher', 
                        'Original', 'Genre', 'Cover' ) )
        {
            print " = ".$field;
            $self->{"is$field"} = 0;
        }
        $self->{nexPage} = '';

        return $html;
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        return "http://katalog.nukat.edu.pl/search/query?term_1=".$word."&theme=nukat";
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;
        return $url if $url;
        return 'http://www.nukat.edu.pl/';
    }

    sub getName
    {
        return "NUKat";
    }

    sub getCharset
    {
        my $self = shift;
        return "UTF-8";
        #return "ISO-8859-2";
    }

    sub getAuthor
    {
        return 'WG - Kerenoc';
    }

    sub getLang
    {
        return 'PL';
    }

    sub getSearchFieldsArray
    {
        return ['isbn', 'title'];
    }

}

1;
