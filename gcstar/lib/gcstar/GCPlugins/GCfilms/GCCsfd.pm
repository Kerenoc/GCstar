package GCPlugins::GCfilms::GCCsfd;

###################################################
#
#  Copyright 2005-2009 Tian
#  Copyright 2007,2011 Petr Gajdůšek <gajdusek.petr@centrum.cz>
#  Copyright 2017-2023 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCfilms::GCfilmsCommon;

{
    package GCPlugins::GCfilms::GCPluginCsfd;

    use base qw(GCPlugins::GCfilms::GCfilmsPluginsBase);

    sub getSearchCharset
    {
        return 'UTF-8';
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;
        return "http://www.csfd.cz/hledat/?q=$word";
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;

        $url = "http://www.csfd.cz" . $url if ($url !~ /^http:/);
        return $url;
    }

    # getCharset
    # Used to convert charset in web pages.
    # Returns the charset as specified in pages.
    #sub getCharset {
    #   my $self = shift;
    #
    #   return "UTF-8";
    #}

    sub getName
    {
        return "CSFD.cz";
    }

    sub getAuthor
    {
        return 'Petr Gajdůšek';
    }

    sub getLang
    {
        return 'CS';
    }

    sub hasSearchYear
    {
        return 1;
    }

    sub hasSearchDirector
    {
        return 1;
    }

    sub hasSearchActors
    {
        return 1;
    }

    # extra field in search list : genres
    sub getExtra
    {

        return 'Žánr';
    }

    sub preProcess
    {
        my ($self, $html) = @_;
        $self->{parsingEnded} = 0;
        if ($self->{parsingList})
        {
            # Search results

            # Initial values for search results parsing
            # There are two movies list:
            # First with detailed info (title, genre, origin country, year, directors, actors)
            # Second with brief list of other movies (title, year)

            # We are in brief list containing other movies without details
            $self->{insideOtherMovies} = 0;
            # Movie link; movie's details follow if not in brief list
            $self->{isMovie} = 0;

            ## Details:

            # Movie's details will follow: Genre, origin, actors, directors, year
            $self->{insideDetails} = 0;
            # In movie's details after paragraph with Genre, origin and date
            $self->{wasDetailsInfo} = 0;
            # In movie's details: directors and actors
            $self->{directors}        = ();
            $self->{directorsCounter} = 0;
            $self->{actors}           = ();
            $self->{actorsCounter}    = 0;
            $self->{insideCreators}   = 0;
            $self->{insideDirectors}  = 0;
            $self->{insideActors}     = 0;

            # Movie year
            $self->{isYear} = 0;
        }
        else
        {
            # Movie page

            # Initial values for search results parsing

            # array containg other movie titles (not exported to GCStar)
            $self->{titles} = ();
            # in list containing other movie titles
            $self->{isTitles} = 0;
            # in the original title (title for same country as movie's origin)
            $self->{isOrigTitle} = 0;
            # original title (if not set during parsing it will be set to main title at the end)
            $self->{origTitle}     = undef;
            $self->{titlesCounter} = 0;

            $self->{insideGenre} = 0;

            $self->{insideSynopsis}   = 0;

            # inside details with country, date (year) and time (length)
            $self->{insideInfo} = 0;

            $self->{insideRating} = 0;

            # User comments
            # Each comment consists of commenter (user) and his comment
            $self->{insideCommentAuthor} = 0;
            $self->{awaitingComment}     = 0;
            $self->{insideComment}       = 0;

            # In directors and actors
            $self->{insideDirectors}  = 0;
            $self->{insideActors}     = 0;
            $self->{directors}        = ();
            $self->{directorsCounter} = 0;
            $self->{actors}           = ();
            $self->{actorsCounter}    = 0;
        }
        return $html;
    }

    # In processing functions below, self->{parsingList} can be used.
    # If true, we are processing a search results page
    # If false, we are processing a movie information page.

    # $self->{inside}->{tagname} (with correct value for tagname) can be used to test
    # if we are in the corresponding tag.

    # You have a counter $self->{movieIdx} that have to be used when processing search results.
    # It is your responsability to increment it!

    # When processing search results, you have to fill (if available) following fields:
    #
    #  $self->{movieList}[$self->{movieIdx}]->{title}
    #  $self->{movieList}[$self->{movieIdx}]->{url}
    #  $self->{movieList}[$self->{movieIdx}]->{actors}
    #  $self->{movieList}[$self->{movieIdx}]->{director}
    #  $self->{movieList}[$self->{movieIdx}]->{date}
    #  $self->{movieList}[$self->{movieIdx}]->{extra}

    # When processing a movie page, you need to fill the fields (if available) in $self->{curInfo}. They are:
    #
    #  $self->{curInfo}->{title}
    #  $self->{curInfo}->{director}
    #  $self->{curInfo}->{original}        (Original title)
    #  $self->{curInfo}->{actors}
    #  $self->{curInfo}->{genre}        (Comma separated list of movie type)
    #  $self->{curInfo}->{country}        (Movie Nationality or country)
    #  $self->{curInfo}->{date}
    #  $self->{curInfo}->{time}
    #  $self->{curInfo}->{synopsis}
    #  $self->{curInfo}->{image}
    #  $self->{curInfo}->{audio}
    #  $self->{curInfo}->{subt}
    #  $self->{curInfo}->{age}          0     : No information
    #                                   1     : Unrated
    #                                   2     : All audience
    #                                   5     : Parental Guidance
    #                                   >= 10 : Minimum age value

    # start
    # Called each time a new HTML tag begins.
    # $tagname is the tag name.
    # $attr is reference to an associative array of tag attributes.
    # $attrseq is an array reference containing all the attributes name.
    # $origtext is the tag text as found in source file
    # Returns nothing
    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;
        $self->{inside}->{$tagname}++;


        if ($self->{parsingEnded})
        {
            return;
        }

        if ($self->{parsingList})
        {

            # in brief list of other movies (without details)
            if ($tagname eq "ul" and $attr->{class} eq "films others")
            {
                $self->{insideOtherMovies} = 1;
            }
            # in link to movie page
            elsif ($tagname eq "a" and $attr->{href} =~ m/\/film\/[0-9]+-.*/)
            {
                $self->{isMovie} = 1;
                return if ( $self->{itemIdx} ne -1 && $self->{itemsList}[ $self->{itemIdx} ]->{url} eq $attr->{href} );
                $self->{itemIdx}++;
                $self->{itemsList}[ $self->{itemIdx} ]->{url} = $attr->{href};
                $self->{insideDetails} = 1 if ($self->{insideOtherMovies} != 1);
                $self->{wasDetailsInfo} = 0;
            }

            # year
            elsif ($tagname eq "span" and $attr->{class} eq "film-title-info")
            {
                $self->{isYear} = 1;
            }

            # creators
            elsif ($tagname eq "p" and $attr->{class} eq "film-creators")
            {
                $self->{insideCreators} = 1;
            }

        }
        else
        {
            # Synopsis
            if (    $tagname eq "div"
                and $attr->{class} eq "plot-full hidden")
            {
                $self->{insideSynopsis}   = 1;
            }

            # Poster
            elsif (    $tagname eq "img"
                and $attr->{src} =~ /\/film\/posters\//)
            {
                $self->{curInfo}->{image} = "http:".$attr->{src};
            }

            # Original name and other names
            elsif ($tagname eq "ul" and $attr->{class} eq "film-names")
            {
                $self->{isTitles} = 1;
            }

            elsif ($tagname eq "img" and $self->{isTitles})
            {
                $self->{isOrigTitle} = 1 if ($attr->{alt} !~ /název$/);
                $self->{isSKTitle}   = 1 if ($attr->{alt} =~ /SK název$/);
            }

            # Genre
            elsif ($tagname eq "div" and $attr->{class} eq "genres")
            {
                $self->{insideGenre} = 1;
            }

            # Info (country ,date, time = duration)
            elsif ($tagname eq "div" and $attr->{class} eq "origin")
            {
                $self->{insideInfo} = 1;
            }

            # Rating
            elsif ($tagname eq "div" and $attr->{class} eq "film-rating-average")
            {
                $self->{insideRating} = 1;
            }

            # Comments
            elsif ($tagname eq "a" and $attr->{class} eq "user-title-name")
            {
                $self->{insideCommentAuthor} = 1;
            }
            elsif ($self->{awaitingComment} and $tagname eq "span" and $attr->{class} eq "comment")
            {
                $self->{awaitingComment} = 0;
                $self->{insideComment}   = 1;
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;
        $self->{inside}->{$tagname}--;

        if ($self->{parsingList})


        {
            # movie details
            if ($tagname eq "div" and $self->{insideDetails})
            {
                $self->{insideDetails} = 0;
            }

            # directors and actors
            elsif ($tagname eq "p")
            {
                if ($self->{insideDirectors})
                {
                    $self->{insideDirectors} = 0;
                    $self->{itemsList}[ $self->{itemIdx} ]->{director} =
                      join(', ', @{$self->{directors}});
                    $self->{directors}        = ();
                    $self->{directorsCounter} = 0;
                }
                elsif ($self->{insideActors})
                {
                    $self->{insideActors} = 0;
                    $self->{itemsList}[ $self->{itemIdx} ]->{actors} =
                      join(', ', @{$self->{actors}});
                    $self->{actors}           = ();
                    $self->{actorsCounter}    = 0;
                }
            }
        }
        else
        {
            # Synopsis
            $self->{insideSynopsis} = 0 if ($tagname eq "div");

            # Titles
            if ($tagname eq "ul" and $self->{isTitles})
            {
                $self->{isTitles} = 0;
            }

            elsif ( $tagname eq "body" )
            {
                $self->{curInfo}->{original} ||= $self->{curInfo}->{title};
            }

            # Actors
            elsif ($tagname eq "div" and $self->{insideActors})
            {
                $self->{curInfo}->{actors} = join(', ', @{$self->{actors}});
                $self->{insideActors} = 0;
            }

            # Directors
            elsif ($tagname eq "div" and $self->{insideDirectors})
            {
                $self->{curInfo}->{director} = join(', ', @{$self->{directors}});
                $self->{insideDirectors} = 0;
            }

            # Comment
            elsif ($tagname eq "h3" and $self->{insideCommentAuthor})
            {
                $self->{insideCommentAuthor} = 0
            }

            elsif ($tagname eq "em" and $self->{insideComment})
            {
                $self->{curInfo}->{comment} .= "\n";
            }

            elsif ($tagname eq "article" and $self->{insideComment})
            {
                $self->{curInfo}->{comment} .= "\n---------------------------------------\n";
                $self->{insideComment} = 0;
            }

            # Debug
            elsif ($tagname eq "html" and $self->{debug})
            {
                use Data::Dumper;
                print Dumper $self->{curInfo};
            }
        }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        return if length($origtext) < 2;
        $origtext =~ s/^\s+|\s+$//g;

        return if ($self->{parsingEnded});
        return if (! $origtext);

        if ($self->{parsingList})
        {
            # Movie title
            if ($self->{isMovie})
            {
                $self->{itemsList}[ $self->{itemIdx} ]->{"title"} = $origtext;
                $self->{isMovie} = 0;
                return;
            }

            # Date (year)
            elsif ($self->{isYear})
            {
                $origtext =~ s/[\)\(]//g;
                $self->{itemsList}[ $self->{itemIdx} ]->{"date"} = $origtext;
                $self->{isYear} = 0;
            }

            # Extra movie info: origin, genre
            elsif ( $self->{inside}->{p}
                and $self->{insideDetails}
                and $self->{wasDetailsInfo} == 0)
            {
                my @tmp = split(', ', $origtext);
                my $pos = $#tmp;
                my ($country, $genre) = (undef, undef, undef);
                $genre = $tmp[$pos] if ($pos >= 0);
                $pos--;
                $country = $tmp[$pos] if ($pos >= 0);

                $self->{itemsList}[ $self->{itemIdx} ]->{country} = $country
                  if (defined $country);
                $self->{itemsList}[ $self->{itemIdx} ]->{extra} = $genre
                  if (defined $genre);
                $self->{wasDetailsInfo} = 1;
            }

            # directors and actors
            elsif ($self->{insideCreators})
            {
                $self->{insideDirectors} = 1 if ($origtext eq "Režie:");
                $self->{insideActors}    = 1 if ($origtext eq "Hrají:");
                $self->{insideCreators} = 0;
            }

            # Directors
            elsif ($self->{inside}->{a} and $self->{insideDirectors})
            {
                push @{$self->{directors}}, $origtext;
                $self->{directorsCounter}++;
            }

            # Actors
            elsif ($self->{inside}->{a} and $self->{insideActors})
            {
                push @{$self->{actors}}, $origtext;
                $self->{actorsCounter}++;
            }
        }
        else
        {

            # Movie titles
            if ($self->{inside}->{h1})
            {
                $self->{curInfo}->{title} = $origtext
                  if !$self->{curInfo}->{title};
            }
            elsif ($self->{isTitles})
            {
                $self->{titlesCounter}++;
                $self->{titles}[ $self->{titlesCounter} ] = $origtext;
                if ($self->{isOrigTitle})
                {
                    $self->{curInfo}->{original} ||= $origtext;
                    $self->{isOrigTitle} = 0;
                }
                if ($self->{isSKTitle} and $self->{lang} eq "SK")
                {
                    $self->{curInfo}->{title} = $origtext;
                    $self->{isSKTitle} = 0;
                }
            }

            # Genre
            elsif ($self->{insideGenre})
            {
                $origtext =~ s/ \/ /,/g;
                $self->{curInfo}->{genre} = $origtext;
                $self->{insideGenre} = 0;
            }

            # Extra movie info: country, date (year), time
            elsif ($self->{insideInfo} eq 1)
            {
                my ($country) = split(', ', $origtext);
                $country =~ s/ \/ .*.*//g;

                $self->{curInfo}->{country} = $country;
                $self->{insideInfo} = 2;
            }
            elsif ($self->{insideInfo} eq 2)
            {
                $origtext =~ s/,.*//;
                $origtext =~ s/[\)\(]//g;
                $self->{curInfo}->{date} = $origtext;
                $self->{insideInfo} = 3;
            }
            elsif ($self->{insideInfo} eq 3)
            {
                if ($origtext =~ m/(\d+) h (\d+) min/ )
                {
                    $self->{curInfo}->{time} = $1*60 + $2;
                }
                elsif ($origtext =~ m/(\d+) min/ )
                {
                    $self->{curInfo}->{time} = $1;
                }
                $self->{insideInfo} = 0;
            }

            # Directors and Actors
            elsif ($self->{inside}->{h4})
            {
                $self->{insideDirectors} = 1 if ($origtext =~ /^Režie:/);
                $self->{insideActors}    = 1 if ($origtext =~ /^Hrají:/);
            }
            elsif ($self->{inside}->{a} and $self->{insideDirectors})
            {
                push @{$self->{directors}}, $origtext;
                $self->{directorsCounter}++;
            }
            elsif ($self->{inside}->{a} and $self->{insideActors} and $origtext ne "více")
            {
                #push @{$self->{curInfo}->{actors}}, [$origtext]
                #  if ($self->{actorsCounter} <
                #    $GCPlugins::GCfilms::GCfilmsCommon::MAX_ACTORS);
                #$self->{actorsCounter}++;
                push @{$self->{actors}}, $origtext;
                $self->{actorsCounter}++;
            }

            # Synopsis
            elsif ($self->{insideSynopsis})
            {
                $self->{curInfo}->{synopsis} .= $origtext . "\n" if $origtext ne '';
            }

            # Rating
            elsif ($self->{insideRating})
            {
                $origtext =~ s/([0-9]+)%/$1/;
                $self->{curInfo}->{ratingpress} = int($origtext / 10 + .5)
                  if ($origtext ne "");
                $self->{insideRating} = 0;
            }

            # Comments
            elsif ($self->{inside}->{a} and $self->{insideCommentAuthor})
            {
                $self->{curInfo}->{comment} .= $origtext . " napsal(a):\n";
                $self->{awaitingComment} = 1;
            }
            elsif ($self->{insideComment})
            {
                $self->{curInfo}->{comment} .= $origtext . " ";
            }
        }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();

        # Do your init stuff here

        bless($self, $class);

        $self->{hasField} = {
            title    => 1,
            date     => 1,
            director => 1,
            actors   => 1,
            country  => 1
        };

        $self->{lang} = "CS";

        $self->{curName} = undef;
        $self->{curUrl}  = undef;

        $self->{debug} = ($ENV{GCS_DEBUG_PLUGIN_PHASE} > 0);

        return $self;
    }
}

1;
