package GCPlugins::GCstar::GCAmazonCommon;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;

use GCPlugins::GCPluginsBase;

{
    package GCPlugins::GCstar::GCPluginAmazonCommon;

    sub text
    {
        my ($self, $origtext) = @_;
        return 0 if ($self->{parsingEnded});

        if ($self->{parsingList})
        {
            if (
                (($self->{inside}->{b})
                || ($self->{inside}->{span})
                || ($self->{inside}->{label}))
               )
            {
                my $suffix = $self->{suffix};
                if ((($suffix =~ /^co/) && ($origtext =~ /Sort by/))
                 || (($suffix eq 'fr' ) && ($origtext =~ /Trier par/))
                 || (($suffix eq 'de' ) && ($origtext =~ /Sortieren nach/)))
                {
                    $self->{beginParsing} = 1;
                    return 1;
                }
            }
        }

        return 0;
    }

    sub extractImage
    {
        my ($self, $attr) = @_;
        my $url = $attr->{src};
        return 'http://images.amazon.com/images/'.$1.'/'.$2.$3.'MZZZZZZZ.'.$5
            if ($url =~ m%^http://.*?images[.-]amazon\.com/images/(P)/([A-Z0-9]*)(\.[0-9]+\.)?[-A-Za-z0-9_.,]*?ZZZZZZZ(.*?)\.(jpg|gif)%);
        return 'http://images.amazon.com/images/'.$1.'/'.$2.'.'.$3
            if ($url =~ m%^http://.*?images[.-]amazon\.com/images/(I|G)/([-\%A-Z0-9a-z+]*)\._.*?_\.(jpg|gif)%);
        if ($attr->{id} eq 'prodImage')
        {
            $url =~ s/_AA[0-9]*_//;
            return $url;
        }
        return '';
    }

    sub isEAN
    {
        my ($self, $value) = @_;

        my $l = length($value);
        return 1
            if ($l == 8)
            || ($l == 13)
            || ($l == 15)
            || ($l == 18);
        return 0;
    }

    sub isItemUrl
    {
        my ($self, $url) = @_;
        return $1
            if (($url =~ m|/dp/[A-Z0-9]*/sr=([0-3]-[0-9]*)/qid=[0-9]*|)
             || ($url =~ m|/dp/[A-Z0-9]*/ref=(?:sr\|pd)_([a-z0-9_]*)/[0-9]*|)
             || ($url =~ m|/gp/offer-listing/[A-Z0-9]*/ref=(?:sr\|pd)_([a-z0-9_]*)/[0-9]*|)
             || ($url =~ m|/dp/[A-Z0-9]*/ref=(?:sr\|pd)_([a-z0-9_]*)/[0-9]*|));
        return undef;
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;
        return "http://www.amazon.".$self->{suffix}."/gp/search/?redirect=true&search-alias=".$self->{searchType}."&keywords=$word";
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;
        return $url if $url;
        return 'http://www.amazon.'.$self->{suffix};
    }

    sub getAuthor
    {
        return 'Tian';
    }

    sub transformPlatform
    {
        my ($self, $platform) = @_;
        
        $platform =~ s/^([\w ]*)\W{2}.*$/$1/ms;
        $platform =~ s/SONY //i;
        if ($platform =~ m/windows/i)
        {
           $platform = 'PC';
        }
        return $platform;
    }

    sub preProcess
    {
        my ($self, $html) = @_;
        if ($self->{parsingList})
        {
            $html =~ s|<span\s+class="srTitle">([^<]*)</span>|<srTitle>$1</srTitle>|gim;
            $html =~ s|<td class="otherEditions">.*?</td>||gim;
        }
        else
        {
            $html =~ s|<a\s*href="/exec/obidos/ASIN/[0-9/\${}]*">([^<]*)</a>|$1|gim;
        }
        return $html;
    }

    sub decodeDate
    {
        my ($self, $date) = @_;


        $date =~ s/^(\d\d\d\d).$/$1/; # Japanese character at the end
        $date =~ s|^(\d\d\d\d)/(\d+)/(\d+)|$3/$2/$1|;
        # date déjà dans le bon format
        return $date if ($date =~ m|/.*/|);
        # année seule
        return "01/01/".$date if ($date =~ m/^[0-9]+$/);
        # pas d'année
        return '' if (! $date =~ m/[0-9][0-9][0-9][0-9]/);
        if ($date =~ m/trimestre/)
        {
            $date =~ s/[^0-9]* *trimestre */ /;
            my @dateItems = split(/\s/, $date);
            $date = "01/01/".$dateItems[1] if ($dateItems[0] eq '1');
            $date = "01/04/".$dateItems[1] if ($dateItems[0] eq '2');
            $date = "01/07/".$dateItems[1] if ($dateItems[0] eq '3');
            $date = "01/10/".$dateItems[1] if ($dateItems[0] eq '4');
            return $date;
        }
        my $lang = $self->getLang();
        $date =~ s/[\(\)]//g;
        $date =~ s/^\s+//;
        $date =~ s/\s+$//g;
        $date =~ s/[\.`,]//g if ($lang ne 'PT');
        $date =~ s/\-\s*//;
        $date =~ s/ de / /g;
        $date = GCUtils::strToTime($date,"%e %B %Y",$lang);
        $date = GCUtils::strToTime($date,"%B %e %Y",$lang);
        $date = GCUtils::strToTime($date,"%e %b %Y",$lang);
        $date = GCUtils::strToTime($date,"%b %e %Y",$lang);
        $date = GCUtils::strToTime($date,"%B %e %Y",$lang);
        $date = GCUtils::strToTime($date,"%b %e %Y",$lang);
        $date = GCUtils::strToTime($date,"%B %Y",   $lang);
        $date = GCUtils::strToTime($date,"%e %b %Y","EN");
        return $date if ($date =~ m|/.*/|);
        $date =~ s/([a-z]+) /$1. /;
        $date = GCUtils::strToTime($date,"%b %e %Y",$lang);
        return $date if ($date =~ m|/.*/|);
        return undef;
    }
}

1;
