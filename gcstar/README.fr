GCstar, gestionnaire de collections personnelles

Application permettant de gérer ses collections.
Plus d'informations : http://www.gcstar.org/ ou https://gitlab.com/GCstar/GCstar

Installation à partir des sources
---------------------------------

Vous devez avoir au préalable installé Gtk3, perl et perl-gtk3.
Puis lancez le script install comme indiqué ci-dessous.
Pour faire une installation sur tout le systeme, lancez le en tant que root.

Pour installer en mode graphique :
./install

Pour installer en mode texte :
./install --text

Pour automatiquement installer dans un répertoire :
./install --prefix=/chemin/vers/le/repertoire


Contribution
------------

Si vous souhaitez contribuer à GCstar, consultez cette page :

https://gitlab.com/GCstar/GCstar


Rapport d'anomalie
------------------

Pour soumettre un rapport d'anomalie, rendez vous ici :

https://gitlab.com/GCstar/GCstar/-/issues

