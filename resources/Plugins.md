# GCstar Plugins
Current status of plugins. A *nok* status means that the plugins is not working as expected but the the website is still available and that the plugin could be repared. A *ko* status means that the web site disappeared. Any help is welcome to improve or fix plugins. 

## Board games

| Plugin        | Lang | Authors           | Status | Comment |
| ------        | ---- |-------            | ------ | ------- |
| BoardGamegeek | EN   | Zombiepig         | ok     |         |
| TricTrac      | FR   | Florent - Kerenoc | ok     |         |
|~~ReservoirJeux~~ | FR   | Florent - Kerenoc | ok     | closed site (removed) |


## Books

| Plugin                | Lang | Authors                       | Status | Comment |
| ------                | ---  | ------                        | -----  | ------- |
| AdLibris (FI)         | FI   | TPF-Kerenoc                   | ok     | | 
| AdLibris (NO)         | NO   | TPF-Kerenoc                   | ok     | | 
| AdLibris (SV)         | SV   | TPF-Kerenoc                   | ok     | | 
| Amazon BR             | PT   | Varkolak-Kerenoc-Knight Rider | ok     | |
| Amazon CA-UK-US       | EN   | Varkolak-Kerenoc              | ok     | |
| Amazon DE             | DE   | Varkolak-Kerenoc              | ok     | |
| Amazon PL             | PL   | Varkolak-Kerenoc              | ok     | |
| Amazon FR-QC          | FR   | Varkolak-Kerenoc              | ok     | |
| Bdphile               | FR   | Jonas-Kerenoc-MNormand        | ok     | |
| Bedetheque            | FR   | Mckmonster-Jpa31-Kerenoc      | to improve | writer search not working |
| BibliotekaNarodowa    | PL   | MW                            | ok     | migration to json API |
| Bokkilden             | NO   | Tian                          | ok     | |
| Chapitre              | FR   | TPF-Kerenoc                   | ok     | |
| Google Books          | EN   | MW                            | ok     | to fix (covers) |
| Le-Livre              | FR   | Varkolak                      | ok     | |
| LeyaOnline            | PT   | Kerenoc                       | ok     | |
| NooSFere              | FR   | Varkolak                      | ok     | |
| NUKat                 | PL   | WG                            | ok     | |
| Bol                   | IT   | TPF-UnclePetros               | nok    | to fix (moved to Mondadori) |
| Buscape               | PT   | TPF                           | nok    | to fix | 
| Casadelibro           | ES   | PPF                           | nok    | to fix | 
| Doubanbook 豆瓣       | ZH   | BW                             | nok    | API v2 requires a key|
| Fnac (FR)             | FR   | TPF - Kerenoc                 | nok    | to fix : protection system |
| Fnac (PT)             | PT   | TPF                           | nok    | to fix |
| InternetBookshop IBS  | IT   | TPF                           | nok    | to fix (moved to IBS.it) |
| ISBNdb                | EN   | TPF                           | nok    | to fix |
| ISFdb                 | EN   |                               | todo   | SiFi community database | 
| ~~Alapage~~           | FR   | TPF                           | ko     | closed site : removed |
| ~~BDGuest~~           | FR   | jpa31                         | ko     | replaced by Bedetheque|
| ~~InternetBokHandeln~~| SV   | TPF                           | ko     | closed site : removed |
| ~~LiberOnWer~~        | IT   | TPF                           | ko     | closed site : removed |
| ~~Mareno~~            | PL   | WG                            | ko     | closed site : removed |
| ~~Mediabooks~~        | PT   | TPF                           | ko     | replaced by LeyaOnline |
| ~~Merlin~~            | PL   | WG                            | ko     | closed site : removed | 
| ~~Saraiva~~           | PT   | Nurev                         | ko     | closed site : removed |

## Building toys
 
| Plugin   | Lang | Authors | Status | Comment |
| -------- | ---- | ------- | ------ | ------- |
| Brickset | EN   | Tian    | ok     |         |


## Coins

| Plugin         | Lang | Authors                      | Status     | Comment                             |
| ------         | ---- | -------                      | ------     | -------                             |
| Numista        | FR   | Michel_P                     | ok         | |


## Comics

| Plugin             | Lang | Authors                  | Status     | Comment                             |
| ------             | ---- | -------                  | ------     | -------                             |
| Amazon BR          | PT   | Varkolak-Kerenoc-Knight Rider | ok    | |
| Amazon CA-UK-US    | EN   | Varkolak-Kerenoc         | ok         | |
| Amazon DE          | DE   | Varkolak-Kerenoc         | ok         | |
| Amazon PL          | PL   | Varkolak-Kerenoc      | ok     | |
| Amazon FR-QC       | FR   | Varkolak-Kerenoc         | ok         | |
| Bdphile            | FR   | Jonas-Kerenoc-MNormand   | ok         | migration from books plugin done    |
| Bedetheque         | FR   | Mckmonster-Jpa31-Kerenoc | to improve | writer search not working           |
| Sanctuary          | FR   | Kerenoc                  | ok         | replaces MangaSanctuary             |
| ~~ComicBookDb~~    | EN   | Zombiepig                | no         | site closed in 2019                 |
| ~~MangaSanctuary~~ | FR   | Biggriffon               | nok        | changes in web site, replaced by Sanctuary |


## Films

| Plugin          | Lang | Authors                     | Status     | Comment |
| ------          | ---- | -------                     | ------     | ------- |
| Allmovie        | EN   | Zombiepig - Kerenoc         | ok         | 2 passes |
| Allocine        | FR   | Tian - Kerenoc              | ok         | 2 passes |
| Amazon DE       | DE   | Tian - Kerenoc              | ok         | common code to be shared |
| Amazon FR       | FR   | Tian - Kerenoc              | ok         | common code to be shared |
| Amazon UK       | EN   | Tian - Kerenoc              | ok         | common code to be shared |
| Amazon US       | EN   | Tian - Kerenoc              | ok         | specific code |
| Animator        | RU   | zserghei                    | ok         | presumed ok, not tested with cyrillic charset |
| AniDB           | EN   | MeV                         | ok         | |
| AnimatorEN      | EN   | zserghei                    | ok         | to improve, minimum English version |
| Animeka.com     | FR   | MeV                         | ok         | |
| Beyaz Perde     | TR   | Zuencap - Kerenoc           | ok         | |
| CSFD            | CZ   | Petr Gajduek                | ok         | |
| Douban 豆瓣     | ZH   | BW                          | nok        | API v2 requires a key |
| DVDEmpire       | EN   | FiXx                        | to improve | lots of missing fields |
| DVDfr           | FR   | MeV                         | ok         | |
| FilmAffinityEN  | EN   | Tian - PIN - FiXx - Kerenoc | ok         | |
| FilmAffinityES  | ES   | Tian - PIN - FiXx - Kerenoc | ok         | |
| FilmUP          | IT   | Tian                        | ok         | |
| FilmWeb         | PL   | Tian - Kerenoc              | to improve | |
| IBS - Internet Bookshop | IT   | t-storm             | ok         | |
| IMDb            | EN   | groms - snaporaz            | ok         | |
| Kinopoisk       | RU   | Nazarov Pavel               | ok         | |
| Moviecovers     | FR   | 2emeagauche - P Fratczak    | ok         | |
| MovieMeter      | NL   | MaTiZ                       | ok         | |
| OdeonHU         | HU   | Anonymous                   | ok         | |
| OFDb            | DE   | MeV                         | ok         | |
| TheMovieDB      | DE EN ES FR | Zombiepig            | ok         | new key for the API v3|
| CinemaClock.com | FR   | MeV                         | nok        | to fix |
| Discshop.se     | SV   | Tian                        | nok        | to fix |
| Port.hu         | HU   | Anonymous                   | nok        | to fix |
| ~~AnimeNfo~~                | EN  | MeV              | ko         | closed site |
| ~~CartelesPeliculas~~       | ES  | DoVerMan         | ko         | closed site |
| ~~CartelesMetropoliGlobal~~ | ES  | DoVerMan         | ko         | closed site |
| ~~CineMotions.com~~         | FR  | MeV              | ko         | closed site |
| ~~CulturaliaNet~~           | ES  | MeV              | ko         | closed site |
| ~~DVDPost~~                 | FR  | MeV - Kerenoc    | ko         | closed site |
| ~~DVDPost.be~~              | EN  | MeV              | ko         | closed site |
| ~~Mediadis~~                | EN  | Tian             | ko         | closed site |
| ~~MonsieurCinema.com~~      | FR  | MeV              | ko         | closed site |
| ~~NasheKino~~               | RU  | zserghei         | ko         | closed site |
| ~~Onet~~                    | PL  | Marek Cendrowicz | ko         | closed site |
| ~~Stopklatka~~              | PL  | Marek Cendrowicz | ko         | site changed |

## Gadgets

| Plugin               | Lang | Authors          | Status | Comment |
| ------               | ---  | ------           | -----  | ------- |
| Amazon BR            | BR   | Varkolak-Kerenoc      | ok     | |
| Amazon CA-UK-US      | EN   | Varkolak-Kerenoc | ok     | |
| Amazon DE            | DE   | Varkolak-Kerenoc | ok     | |
| Amazon PL            | PL   | Varkolak-Kerenoc      | ok     | |
| Amazon FR-QC         | FR   | Varkolak-Kerenoc | ok     | |
| UPC item DB          | EN   | Kerenoc          | ok     | trial API, limit on calls per day |
 
## Music

| Plugin      | Lang | Authors | Status | Comment         |
| ------      | ---- | ------- | ------ | -------         |
| Douban      | ZH   | BW      | ok     |                 |
| MusicBrainz | EN   | Tian    | ok     |                 |
| Discogs     | FR   | TPF     | nok    | API keys to get |


## Music Courses

| Plugin      | Lang | Authors | Status | Comment         |
| ------      | ---- | ------- | ------ | -------         |
| Alfred      | EN   | FiXx    | ok     |                 |


## TV Episodes

| Plugin       | Lang        | Authors   | Status | Comment               |
| ------       | ----        | -------   | ------ | -------               |
| Tvdb         | EN ES FR IT | Zombiepig | ok     |                       |
| TheMovieDB   | DE EN ES FR | Kerenoc   | ok     | new key for the API v3|


## TV Series

| Plugin       | Lang         | Authors             | Status | Comment |
| ------       | ----         | -------             | ------ | ------- |
| Allocine     | FR           | Tian - Kerenoc      | ok     | limited initial implementation |
| Beyaz Perde  | TR           | Kerenoc             | ok     | |
| Imdb         | EN           | Kerenoc             | testing |         |
| TheMovieDB   | DE EN ES FR  | Zombiepig - Kerenoc | ok     |         |
| Tvdb         | EN ES FR IT  | Zombiepieg          | ok     |         |


## Video games
    
| Plugin        | Lang | Authors        | Status | Comment |
| ------        | ---- | -------        | ------ | ------- |
| Amazon (BR)   | PT   | TPF-Kerenoc    | ok     |         |
| Amazon (CA)   | EN   | TPF-Kerenoc    | ok     |         |
| Amazon (DE    | DE   | TPF-Kerenoc    | ok     |         |
| Amazon (FR)   | FR   | TPF-Kerenoc    | ok     |         |
| Amazon (JA)   | JA   | TPF-Kerenoc    | ok     |         |
| Amazon (PL)   | PL   | TPF-Kerenoc    | ok     |         |
| Amazon (QC)   | FR   | TPF-Kerenoc    | ok     |         |
| Amazon (UK)   | EN   | TPF-Kerenoc    | ok     |         |
| Amazon (US)   | EN   | TPF-Kerenoc    | ok     |         |
| GameSpot      | EN   | Tian - Kerenoc | ok     |         |
| JeuxVideoCom  | FR   | Tian - TPF     | ok     | require cookies.txt |
| MobyGames     | EN   | TPF            | ok     |         |
| JeuxVideoFr   | FR   | Tian           | nok    | some pages are broken |
| NextGames     | IT   | TPF            | nok    | site moved to videogames.it |
| VGCollect     | EN   | Gimont         | ok     |         |
| ~~Alapage~~   | FR   | TPF            | ko     |         |
| ~~DicoDuNet~~ | FR   | TPF            | ko     |         |
| ~~Ludus~~     | IT   | TPF            | ko     |         |
|~~TheLegac~~   | DE   | TPF            | ko     | site closed before GDPR enforcement |
