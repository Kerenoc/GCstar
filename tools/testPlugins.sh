#! bash
#
# testPlugins.sh
#
# script to execute some GCstar commands to check that some plugins are still working
#

if [ -x ${GCSTAR+x} ]
then
    echo "Environment variables to set : GCSTAR GCSTAR_LOG TEMP PERL_BIN"
    exit
fi

# location of GCstar main program
#GCSTAR=
# directory for GCstar log
#GCSTAR_LOG=
# direction for temporay files and cached files
#TEMP=
# normal test => 1, debug of the test script => 3
GCS_DEBUG_PLUGIN_PHASE=3

alias perl="$PERL_BIN"

count_file()
{
    if test -f $1
    then
        extension="${1##*.}"
        case $extension in
            "gcs")
                echo $(grep -E 'title=|name=' $1 | wc) $1
                ;;
            "html")
                echo $(grep '<h2>' $1 | wc) $1
                ;;
            *)
                echo $(cat $1 | wc) $1
                ;;
        esac
    else
        echo "#### ERROR #### " $1
    fi
}

merge_files()
{
    echo "=============================================================================="
    echo '=====     Merging ' $1 $2
    echo ''

    # collection type
    C=$2
    # root for test files
    R=Test_$1
    # import type
    I=$3

    shift; shift ; shift

    if [ "$1" = "-f" ]
    then
        shift
        FILES=$*
    else
        FILES=""
        for i in $*
        do
            FILES="$FILES ${R}_$i.gcs"
        done
    fi

    perl "$GCSTAR" -x --collection $C -o ${R}_all.gcs -i $I $FILES
    perl "$GCSTAR" -x -e CSV \
         -o ${R}_all.csv ${R}_all.gcs

    echo "______________________________________________________________________________"
    echo "Merged file"

    count_file ${R}_all.gcs
    count_file ${R}_all.csv

    echo ""
    echo ""
    echo ""
}

test_site() {
    echo "=============================================================================="
    echo '=====     ' $3 : $5
    echo ''

    # collection type
    C=$2
    # root for test files
    R=Test_$1_$3

    touch ${R}_
    rm -r ${R}_* 2>&1 > /dev/null

    sleep 1

    # test download from web site
    perl "$GCSTAR" -x --collection $C -w "$4" -o ${R}.gcs --download "$5"

    # test saving a gcs file
    perl "$GCSTAR" -x --collection $C \
         -o ${R}_new.gcs ${R}.gcs

    # test export to HTML
    perl "$GCSTAR" -x --collection $C -e HTML \
         --exportprefs "template=>Shelf,title=>$1" \
         -o ${R}.html ${R}.gcs

    # test export to GCZ
    perl "$GCSTAR" -x -e External --exportprefs "zip=>1" \
         -o ${R}_export.gcz  ${R}.gcs

    # test export to CSV
    perl "$GCSTAR" -x -e CSV \
         -o ${R}.csv ${R}.gcs

    # test export to Tellico
   perl "$GCSTAR" -x -e Tellico \
         -o ${R}.tc ${R}.gcs

    # test download and export to HTML
    perl "$GCSTAR" -x --collection $C -w "$4" -e HTML \
         --exportprefs "template=>Shelf,title=>My Collection" \
         -o ${R}_new.html --download "$5"

    # test import from Tellico
    perl "$GCSTAR" -x --collection $C -i Tellico \
         -o ${R}_Tellico.gcs ${R}.tc

    echo "______________________________________________________________________________"
    echo "GCS files"
    count_file ${R}.gcs
    count_file ${R}_new.gcs
    count_file ${R}_Tellico.gcs
    echo "______________________________________________________________________________"
    echo "HTML files"
    count_file ${R}.html
    count_file ${R}_new.html
    echo "______________________________________________________________________________"
    echo "CSV file"
    count_file ${R}.csv
    echo "________________________________________"
    echo "Tellico file"
    count_file ${R}.tc
    echo ""
    echo ""
}

collection_header() {
    echo "=============================================================================="
    echo "=====  $1  ====="
    echo "=============================================================================="
    echo ""
}

test_Books() {
    collection_header "BOOKS                                                           "
    test_site Books GCbooks "AdlibrisFI"         "Adlibris (FI)"       "Harry Potter"  # https://www.adlibris.com/fi/haku?q=harry+potter
    test_site Books GCbooks "AdlibrisNO"         "Adlibris (NO)"       "Harry Potter"  # https://www.adlibris.com/fi/haku?q=harry+potter
    test_site Books GCbooks "AdlibrisSV"         "Adlibris (SV)"       "Harry Potter"  # https://www.adlibris.com/se/sok?q=harry+potter
    test_site Books GCbooks "AmazonCA"           "Amazon (CA)"         "Harry Potter"
    test_site Books GCbooks "AmazonDE"           "Amazon (DE)"         "Das Jesus Video"
    test_site Books GCbooks "AmazonFR"           "Amazon (FR)"         "A la recherche du temps perdu"
    test_site Books GCbooks "AmazonUK"           "Amazon (UK)"         "David Copperfield"
    test_site Books GCbooks "AmazonUS"           "Amazon (US)"         "East of Eden"
    test_site Books GCbooks "Bdphile"            "Bdphile"             "Aquablue"
    test_site Books GCbooks "BibliotekaNarodowa" "Biblioteka Narodowa" "Harry Potter" # http://alpha.bn.org.pl/search*pol
    test_site Books GCbooks "Bokkilden"          "Bokkilden"           "Harry Potter"
    test_site Books GCbooks "Chapitre.com"       "Chapitre.com"        "Harry Potter"
    test_site Books GCbooks "GBooks"             "Google Books"        "Harry Potter"
    test_site Books GCbooks "Le-Livre"           "Le-Livre"            "Harry Potter"
    test_site Books GCbooks "LeyaOnline"         "LeyaOnline"          "Harry Potter"
    test_site Books GCbooks "NooSFere"           "NooSFere"            "Harry Potter"
    test_site Books GCbooks "NUKat"              "NUKat"               "Harry Potter"  # http://katalog.nukat.edu.pl/search/query?term_1=harry+potter&theme=nukat

    # plugins that don't work but may be fixed (website available)
    #test_site Books GCbooks "Bol"                "Bol"                 "Harry Potter"  # http://www.mondadoristore.it/search/?g=Harry+potter&crc=600&bld=15&swe=N&escal=S&accum=N
    #test_site Books GCbooks "Buscape"            "Buscape"             "Harry Potter"  # http://www.buscape.com.br/livros/harry-potter
    #test_site Books GCbooks "Casadelibro"        "Casadelibro"         "Harry Potter"  # https://www.casadellibro.com/libros-harry-potter?query=harry%20potter
    #test_site Books GCbooks "DoubanBook"         "Douban Book"         "Harry Potter"  # https://api.douban.com/v2/book/search?q=harry+potter&fields=id,title&apiKey=0ac44ae016490db2204ce0a042db2916
    #test_site Books GCbooks "FnacFR"             "Fnac (FR)"           "Harry Potter"  # https://www.fnac.com/SearchResult/ResultList.aspx?Search=silo&sft=1&sa=0
    #test_site Books GCbooks "InternetBookShop"   "InternetBookShop"    "Harry Potter"  # https://www.ibs.it/algolia-search?ts=as&query=harry%20potter&query_seo=harry%20potter&qs=true
    #test_site Books GCbooks "ISBNdb"             "ISBNdb"              "9789544464912" # http://isbndb.com/search/all?query=9789544464912

    merge_files Books GCBooks GCstar -f Test_Books_*_new.gcs
}

test_Films() {
    collection_header "FILMS                                                           "
    test_site Films GCfilms Allmovie          "Allmovie"                "Avatar"
    test_site Films GCfilms Allocine          "Allocine"                "Avatar"
    test_site Films GCfilms AmazonDE          "Amazon (DE)"             "Avatar"
    test_site Films GCfilms AmazonFR          "Amazon (FR)"             "Avatar"
    test_site Films GCfilms AmazonUK          "Amazon (UK)"             "Avatar"
    test_site Films GCfilms AmazonUS          "Amazon (US)"             "Avatar"
    test_site Films GCfilms AniDB             "AniDB"                   "Avatar"
    test_site Films GCfilms Animator          "Animator"                "Ava"
    test_site Films GCfilms AnimatorEN        "AnimatorEN"              "PEK"
    test_site Films GCfilms Animeka           "Animeka.com"             "Avatar"
    test_site Films GCfilms BeyazPerde        "Beyaz Perde (TR)"        "Avatar"
    test_site Films GCfilms CSFD              "CSFD.cz"                 "Avatar"
    test_site Films GCfilms Douban            "豆瓣 - Douban"            "Avatar"
    test_site Films GCfilms DVDEmpire         "DVDEmpire (EN)"          "Avatar"
    test_site Films GCfilms FilmAffinityEN    "Film affinity (EN)"      "Avatar"
    test_site Films GCfilms FilmAffinityES    "Film affinity (ES)"      "Avatar"
    test_site Films GCfilms FilmUP            "FilmUP"                  "Avatar"
    test_site Films GCfilms FilmWeb           "FilmWeb"                 "Avatar"
    test_site Films GCfilms InternetBookshop  "IBS - Internet Bookshop" "Avatar"
    test_site Films GCfilms IMDb              "IMDb"                    "Avatar"
    test_site Films GCfilms Kinopoisk         "Kinopoisk"               "Avatar"
    test_site Films GCfilms MovieCovers       "MovieCovers.com"         "Avatar"
    test_site Films GCfilms MovieMeter        "MovieMeter.nl"           "Avatar"
    test_site Films GCfilms OdeonHU           "Odeon.hu"                "Avatar"
    test_site Films GCfilms OFDd              "OFDb.de"                 "Avatar"
    test_site Films GCfilms TheMovieDB        "The Movie DB"            "Avatar"
    test_site Films GCfilms TheMovieDBDE      "The Movie DB (DE)"       "Avatar"
    test_site Films GCfilms TheMovieDBES      "The Movie DB (ES)"       "Avatar"
    test_site Films GCfilms TheMovieDBFR      "The Movie DB (FR)"       "Avatar"

    # plugins that don't work but may be fixed (website available)
    #test_site Films GCfilms CinemaClock "CinemaClock.com" "Avatar"
    #test_site Films GCfilms Discshop "Discshop.se" "Avatar"
    #test_site Films GCfilms PortHU "port.hu" "Avatar"

    # sites that closed or changed
    # Alapage Alpacine AnimeNfo CartelesMetropoliGlobal CartelesPeliculas CulturaliaNet Cinemotions 
    # DVDPost Mediadis Metropoli MonsieurCinema Onet Stopklatka

    merge_files Films GCfilms GCstar -f Test_Films_*_new.gcs
}

test_Comics() {
    collection_header "COMICS
                                    "
    test_site Comics GCcomics Bdphile        "Bdphile"          "Aquablue"
    test_site Comics GCcomics Bedetheque     "Bedetheque"       "Aquablue"
    test_site Comics GCcomics Sanctuary      "Sanctuary"        "Mushishi"
    test_site Comics GCcomics AmazonCA       "Amazon (CA)"      "Mushishi"
    test_site Comics GCcomics AmazonDE       "Amazon (DE)"      "Mushishi"
    test_site Comics GCcomics AmazonFR       "Amazon (FR)"      "Mushishi"
    test_site Comics GCcomics AmazonPL       "Amazon (QC)"      "Mushishi"
    test_site Comics GCcomics AmazonQC       "Amazon (QC)"      "Mushishi"
    test_site Comics GCcomics AmazonUK       "Amazon (UK)"      "Mushishi"
    test_site Comics GCcomics AmazonUS       "Amazon (US)"      "Mushishi"

    #test_site Comics GCcomics ComicBookDB    "Comic Book DB"   "Before Watchmen" # site closed in 2019

    merge_files Comics GCcomics GCstar -f Test_Comics_*_new.gcs
}

test_BuildingToys() {
    collection_header "BUILDING TOYS                                                   "

    test_site BuildingToys GCbuildingtoys Brickset_Skywalker "Brickset" Skywalker
    test_site BuildingToys GCbuildingtoys Brickset_7962      "Brickset" 7962

    merge_files BuildingToys GCbuildingtoys GCstar -f Test_BuildingToys_*_new.gcs
}

test_Music() {
    collection_header "MUSIC                                                           "

    test_site Music GCmusics Discogs "Discogs" "Eliminator"
    test_site Music GCmusics MusicBrainz "MusicBrainz" "Outlandos"

    #test_site Music GCmusics MusicBrainz "MusicBrainz" "Eliminator" # Tellico file too big
    #test_site Music GCmusics DoubanMusic "Doubanmusic" "Eliminator" # requires a patch to GCDoubanmusic.pm

    merge_files Music GCmusics GCstar -f Test_Music_*_new.gcs
}

test_VideoGames() {
    collection_header "VIDEO GAMES                                                     "

    test_site VideoGames GCgames AmazonCA     "Amazon (CA)"   "Uncharted"
    test_site VideoGames GCgames AmazonDE     "Amazon (DE)"   "Uncharted"
    test_site VideoGames GCgames AmazonFR     "Amazon (FR)"   "Uncharted"
    test_site VideoGames GCgames AmazonJP     "Amazon (JP)"   "Uncharted"
    test_site VideoGames GCgames AmazonUK     "Amazon (UK)"   "Uncharted"
    test_site VideoGames GCgames AmazonUS     "Amazon (US)"   "Uncharted"
    test_site VideoGames GCgames Gamespot     "GameSpot"      "Ultima"
    test_site VideoGames GCgames JeuxVideoCom "jeuxvideo.com" "Uncharted"
    test_site VideoGames GCgames MobyGames    "MobyGames"     "Uncharted"

    # plugins that don't work but may be fixed (website available)
    # test_site VideoGames GCgames JeuxVideoFR  "jeuxvideo.fr"  "Uncharted"
    # test_site VideoGames GCgames NextGame     "NextGame"      "Uncharted"

    # plugin for sites that closed
    # Alapage, DicoDuNet, Ludus
    # test_site VideoGames GCgames TheLegacy    "TheLegacy"     "Keen"

    merge_files VideoGames GCgames GCstar -f Test_VideoGames_*_new.gcs
}

test_BoardGames() {
    collection_header "BOARD GAMES                                                     "

    test_site BoardGames GCboardgames BoardGameGeek "Board Game Geek" "Carcassonne"
    test_site BoardGames GCboardgames TricTrac      "Tric Trac"       "Carcassonne"

    # test_site BoardGames GCboardgames ReservoirJeux "Reservoir Jeux"  "Carcassonne"

    merge_files BoardGames GCboardgames GCstar -f Test_BoardGames_*_new.gcs
}

test_Gadgets() {
    collection_header "GADGETS                                                         "

    test_site Gadgets GCgadgets AmazonCA_Clock      "Amazon (CA)" "4905524962031" # Clock
    test_site Gadgets GCgadgets AmazonCA_Earphones  "Amazon (CA)" "4905524727685" # Earphones
    test_site Gadgets GCgadgets AmazonCA_Camera     "Amazon (CA)" "0074101037722" # Camera

    merge_files Gadgets_AmazonCA GCgadgets GCstar Clock Earphones Camera

    test_site Gadgets GCgadgets AmazonDE_Clock      "Amazon (DE)" "4905524962031" # Clock
    test_site Gadgets GCgadgets AmazonDE_Earphones  "Amazon (DE)" "4905524727685" # Earphones

    merge_files Gadgets_AmazonDE GCgadgets GCstar Clock Earphones

    test_site Gadgets GCgadgets AmazonFR_Earphones  "Amazon (FR)" "4905524727685" # Earphones
    test_site Gadgets GCgadgets AmazonFR_Camera     "Amazon (FR)" "4548736014367" # Camera
    test_site Gadgets GCgadgets AmazonFR_Disk       "Amazon (FR)" "0763649091173" # Disk

    merge_files Gadgets_AmazonFR GCgadgets GCstar Earphones Camera Disk

    test_site Gadgets GCgadgets AmazonUK_Clock      "Amazon (UK)" "4905524962031" # Clock
    test_site Gadgets GCgadgets AmazonUK_Earphones  "Amazon (UK)" "4905524727685" # Earphones
    test_site Gadgets GCgadgets AmazonUK_Camera     "Amazon (UK)" "4548736014367" # Camera

    merge_files Gadgets_AmazonUK GCgadgets GCstar Clock Earphones Camera

    test_site Gadgets GCgadgets AmazonUS_Clock      "Amazon (US)" "4905524962031" # Clock
    test_site Gadgets GCgadgets AmazonUS_Earphones  "Amazon (US)" "4905524727685" # Earphones
    test_site Gadgets GCgadgets AmazonUS_Camera "Amazon (US)" "0074101037722"     # Camera

    merge_files Gadgets_AmazonUS GCgadgets GCstar Clock Earphones Camera

    test_site Gadgets GCgadgets UPCitemDB_Clock      "UPC item DB" "4905524962031" # Clock
    test_site Gadgets GCgadgets UPCitemDB_Earphones  "UPC item DB" "4905524727685" # Earphones
    test_site Gadgets GCgadgets UPCitemDB_Camera     "UPC item DB" "4548736014367" # Camera
    test_site Gadgets GCgadgets UPCitemDB_Disk       "UPC item DB" "0763649091173" # Disk
    test_site Gadgets GCgadgets UPCitemDB_Camera_bis "UPC item DB" "0074101037722" # Camera bis

    merge_files Gadgets_UPCitemDB GCgadgets GCstar Clock Earphones Camera Disk Camera_bis

    merge_files Gadgets GCgadgets GCstar -f Test_Gadgets_*_new.gcs
}

test_Coins() {
    collection_header "COINS                                                         "

    test_site Coins GCcoins NumistaEN_Bohr          "Numista (EN)"     "Niels Bohr"
    test_site Coins GCcoins NumistaFR_Krugerrand    "Numista (FR)"     "krugerrand"
    test_site Coins GCcoins ExonumiaFR_Mickey       "Exonumia (FR)"    "Mickey"
    test_site Coins GCcoins ExonumiaEN_Napoleon     "Exonumia (EN)"    "Napoleon"
    test_site Coins GCcoins NumistaEN_Margrethe     "Numista (EN)"     "Margrethe"
    test_site Coins GCcoins NumistaEN_Pattern       "Numista (EN)"     "Margrethe Pattern"
    test_site Coins GCcoins NumistaFR_Pattern       "Numista (FR)"     "Margrethe Essai"

    merge_files Coins GCcoins GCstar -f Test_Coins_*_new.gcs
}

test_Coins_Search() {
    FileName=Test_Coin_${1// /_}.gcs
    echo $FileName
    perl "$GCSTAR" -x --collection GCcoins -w "$2" -o $FileName --download "$1"
    cat $FileName >> Tests_Coins.gcs
    wc $FileName
}

test_Coin() {
    # manual tests
    collection_header "COINS"

    rm Tests_Coins.gcs
    test_Coins_Search "krugerrand"                          "Numista (FR)"
    test_Coins_Search "Margrethe"                           "Numista (EN)"
    test_Coins_Search "Mickey"                              "Exonumia (FR)"
    test_Coins_Search "Napoleon"                            "Exonumia (EN)"

    merge_files Coins GCcoins GCstar -f Test_Coin_*.gcs

    return
    test_Coins_Search "10 Escudos"                          "Numista (EN)"
    test_Coins_Search "300 Euros"                           "Numista (FR)"
    test_Coins_Search "277 Euros"                           "Numista (FR)"
    test_Coins_Search "Hercule"                             "Numista (FR)"
    test_Coins_Search "Mont Saint Michel"                   "Exonumia (FR)"
    test_Coins_Search "jeton touristique monnaie de paris"  "Exonumia (FR)"
    test_Coins_Search "doudou"                              "Exonumia (FR)"
    test_Coins_Search "Obama"                               "Exonumia (FR)"
    test_Coins_Search "10 Euros"                            "Numista (FR)"
    test_Coins_Search "25 Euros"                            "Numista (FR)"
    test_Coins_Search "Taiwan"                              "Numista (FR)"
    test_Coins_Search "grande muraille de chine"            "Exonumia (FR)"
    test_Coins_Search "tricentenaire connecticut"           "Exonumia (FR)"
    test_Coins_Search "napoleon III tete nue"               "Numista (FR)"
    test_Coins_Search "comte de toulouse"                   "Numista (FR)"
    test_Coins_Search "afghani Amanullah"                   "Numista (FR)"
    test_Coins_Search "thai national bank"                  "Numista (FR)"
    test_Coins_Search "Muhammad al Amin"                    "Numista (FR)"
    test_Coins_Search "vatican"                             "Numista (FR)"
    test_Coins_Search "salva"                               "Numista (FR)"
    test_Coins_Search "grosso"                              "Numista (FR)"
    test_Coins_Search "lirot"                               "Numista (FR)"
    test_Coins_Search "holstein"                            "Numista (FR)"
    test_Coins_Search "prince regent"                       "Numista (FR)"
    test_Coins_Search "albert II effigie"                   "Numista (FR)"
    test_Coins_Search "louisiane"                           "Numista (FR)"
    test_Coins_Search "53383"                               "Numista (FR)"
}

test_Episodes() {
    collection_header "EPISODES                                                     "

    test_site Episodes GCTVepisodes TheMovieDB_DE "The Movie DB (DE)" "Good Omens"
    test_site Episodes GCTVepisodes TheMovieDB_EN "The Movie DB (EN)" "Good Omens"
    test_site Episodes GCTVepisodes TheMovieDB_ES "The Movie DB (ES)" "Good Omens"
    test_site Episodes GCTVepisodes TheMovieDB_FR "The Movie DB (FR)" "Good Omens"
    test_site Episodes GCTVepisodes Tvdb_EN       "Tvdb"              "Good Omens"
    test_site Episodes GCTVepisodes Tvdb_ES       "Tvdb ES"           "Good Omens"
    test_site Episodes GCTVepisodes Tvdb_FR       "Tvdb FR"           "Good Omens"
    test_site Episodes GCTVepisodes Tvdb_IT       "Tvdb IT"           "Good Omens"

    merge_files Episodes GCTVepisodes GCstar -f Test_Episodes_*_new.gcs
}

test_Series() {
    collection_header "SERIES                                                     "

    test_site Series GCTVseries Allocine      "Allocine"          "Good Omens"
    test_site Series GCTVseries BeyazPerde    "Beyaz Perde (TR)"  "Breaking Bad"
    test_site Series GCTVseries IMDb          "IMDb"              "Good Omens"
    test_site Series GCTVseries TheMovieDB_DE "The Movie DB (DE)" "Good Omens"
    test_site Series GCTVseries TheMovieDB_EN "The Movie DB (EN)" "Good Omens"
    test_site Series GCTVseries TheMovieDB_ES "The Movie DB (ES)" "Good Omens"
    test_site Series GCTVseries TheMovieDB_FR "The Movie DB (FR)" "Good Omens"
    test_site Series GCTVseries Tvdb_EN       "Tvdb"              "Good Omens"
    test_site Series GCTVseries Tvdb_ES       "Tvdb ES"           "Good Omens"
    test_site Series GCTVseries Tvdb_FR       "Tvdb FR"           "Good Omens"
    test_site Series GCTVseries Tvdb_IT       "Tvdb IT"           "Good Omens"

    merge_files Series GCTVseries GCstar -f Test_Series_*_new.gcs
}

test_export () {
    extension="${2,,}"
    perl "$GCSTAR" -x -c $1 -e $2 -o Test_$3.$extension Test_$3.gcs
}

test_Exports () {
    test_export TVseries     CSV Series_Dvdb
    test_export TVepisodes   CSV Episodes_Dvdb
    test_export Buildingtoys CSV Construction
    test_export Wines        CSV Wines
    test_export Software     CSV Software
    test_export Minicars     CSV Minicars
    test_export Periodicals  CSV Periodicals
    test_export Coins        CSV Coins
}

# test a web site
 if  [ $# -eq 5 ]
 then
        test_site $1 $2 $3 "$4" "$5"
        exit
 fi

 # test for a collection type
 if  [ $# -eq 1 ]
 then
        test_$1
        exit
 fi

# test everything

test_BoardGames
test_Books
test_BuildingToys
test_Coins
test_Comics
test_Episodes
test_Films
test_Gadgets
test_Music
test_Series
test_VideoGames

test_Exports


