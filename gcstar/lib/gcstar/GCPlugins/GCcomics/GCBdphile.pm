package GCPlugins::GCcomics::GCBdphile;

###################################################
#
#  Copyright 2005-2007 Jonas
#  Copyright 2016-2019 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCcomics::GCcomicsCommon;
use GCPlugins::GCbooks::GCBdphile;

{
    package GCPlugins::GCcomics::GCPluginBdphile;

    use base 'GCPlugins::GCbooks::GCPluginBdphile';
    use base qw(GCPlugins::GCcomics::GCcomicsPluginsBase);

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();

        bless ($self, $class);

        GCPlugins::GCcomics::GCcomicsPluginsBase::initFieldNames($self);
        $self->initParams;

        return $self;
    }

    sub getSearchFieldsArray
    {
        return ['series', 'title', 'writer', 'isbn'];
    }

    sub initParams
    {
        my $self = shift;

        $self->{hasField} = {
            series => 1,
            title => 1,
            writer => 0,
            illustrator => 0,
            publisher => 0,
            publication => 1,
            format => 0,
            numberboards => 0,
            edition => 0,
            web => 0,
            description => 0,
            isbn => 0,
        };

        $self->{lastPass} = 0;

        $self->{searchField} = ($self->getSearchFieldsArray())[0][0]
             if (! $self->{searchField});
    }
}

1;
