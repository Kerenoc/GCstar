image: alpine:latest

stages:
    - prepare
    - build
    - test
    - deploy

digests:
    stage: prepare
    when: manual
    script:
        - echo "Computing digests"
        - ls -l .
        - cd gcstar/lib/gcstar
        - ls -l ../../../tools
        - sh ../../../tools/computeDigests.sh
        - mv list_file_digests.txt ../../..
    artifacts:
        name: "digests"
        paths:
        - list_file_digests.txt
    
linux_packages:
    stage: build
    image: bitnami/minideb:latest
    when: manual
    script:
        - echo "Start building Debian package"
        - apt update
        - apt-get -y install git build-essential debhelper quilt rpm
        - apt-get -y install perl
        - apt-get -y install libgtk3-perl
        - apt-get -y install libgtk3-simplelist-perl
        - apt-get -y install libxml-simple-perl
        - ls -l gcstar/packages/debian
        - cp -r gcstar/packages/debian .
        - ls -l debian
        # append short commit sha to version number if not already done
        - shortsha=$(git rev-parse --short HEAD)
        - export CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA:-$shortsha}
        - sed "/my \$VERSION[^-]*;/s/';/-$CI_COMMIT_SHORT_SHA';/" < gcstar/bin/gcstar > tmp.txt
        - mv tmp.txt gcstar/bin/gcstar
        - export version=$(grep "my  *\$VERSION *=" gcstar/bin/gcstar | sed "s/.*'\(.*\)';.*/\1/")
        - echo "version=$version"
        - chmod +x gcstar/install
        # generate DEB package
        - dpkg-buildpackage -rfakeroot -b -nc -uc -us --no-sign
        - mv ../*.deb .
        # generate RPM package
        - mkdir -p RPM/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
        - tar cfz RPM/SOURCES/GCstar-v1.8.0.tar.gz gcstar
        - cp RPM/SOURCES/GCstar-v1.8.0.tar.gz gcstar_1.8.0-latest.tar.gz
        - ls -l RPM/SOURCES
        - cp gcstar/packages/rpm/gcstar.spec RPM/SPECS
        - cd RPM
        - rpmbuild --define "_topdir `pwd`" -v -ba SPECS/gcstar.spec
        - cp RPMS/noarch/*.rpm ..
        - cd ..
        - ls -l
        - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file gcstar*.deb "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/GCstar/1.8.0/gcstar_1.8.0_-debian_all.deb?select=package_file"'
        - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file gcstar*.rpm "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/GCstar/1.8.0/gcstar_1.8.0-1.noarch.rpm?select=package_file"'
        - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file gcstar*.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/GCstar/1.8.0/gcstar_1.8.0-latest.tar.gz?select=package_file"'
    artifacts:
        name: "linux_packages"
        paths:
        - gcstar*.tar.gz
        - gcstar*.deb
        - gcstar*.rpm

debian_package_test:
    stage: test
    image: bitnami/minideb:latest
    needs:
        - linux_packages
    when: manual
    script:
        - ls -l gcstar*.deb
        - apt install ./gcstar*.deb
        - which gcstar

.windows_job:
    # initialisation of Windows/MSYS2 environment
    stage: build
    tags:
        - saas-windows-medium-amd64
    before_script:
        - wget -O msys2.exe https://github.com/msys2/msys2-installer/releases/download/nightly-x86_64/msys2-base-x86_64-latest.sfx.exe
        - .\msys2.exe -y -oC:\
        - C:\msys64\usr\bin\bash -lc ' '
        - C:\msys64\usr\bin\bash -lc 'pacman --noconfirm -Syuu'  # Core update 
        - C:\msys64\usr\bin\bash -lc 'pacman --noconfirm -Syuu'  # Normal update
        - $env:CHERE_INVOKING = 'yes'                            # Preserve the current working directory
        - $env:MSYSTEM = 'MINGW64'                               # Start a 64 bit Mingw environment

windows_package:
    # create an archive of the execution runtime (Perl+Gtk) and generate a installer program
    extends: .windows_job
    when: manual
    script:
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh msys2_packages'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh perl_dependencies'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh optional_dependencies'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh custom_libgd'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh optional_GD_dependencies'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh prepare_dependencies'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh prepare_files'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh create_installer'
    artifacts:
        name: "msys2_packages"
        paths:
        - GCstar*.exe
        - GCstar_version.txt
        - GCstar_win32.tgz
        when: always

windows_package_latest:
    extends: .windows_job
    when: manual
    script:
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh restore_dependencies'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh prepare_files'
        - C:\msys64\usr\bin\bash -lc 'sh -x tools/build-GCstar-mingw64.sh create_installer'
    artifacts:
        name: "msys2_packages"
        paths:
        - GCstar*.exe
        - GCstar_version.txt
        when: always

.windows_upload_job:
    stage: deploy
    image: alpine:latest
    script:
        - apk -U update
        - apk add --no-cache curl
        - sh -x tools/build-GCstar-mingw64.sh upload_installer

windows_upload:
    extends: .windows_upload_job
    needs:
        - windows_package

windows_upload_latest:
    extends: .windows_upload_job
    needs:
        - windows_package_latest
