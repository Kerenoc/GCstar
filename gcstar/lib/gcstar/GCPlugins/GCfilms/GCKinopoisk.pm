package GCPlugins::GCfilms::GCKinopoisk;

###################################################
#
#  Copyright 2005-2010 Nazarov Pavel
#  Copyright 2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;
use Encode qw(encode);

use GCPlugins::GCfilms::GCfilmsCommon;

{
    package GCPlugins::GCfilms::GCPluginKinopoisk;

    use base qw(GCPlugins::GCfilms::GCfilmsPluginsBase);

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        return if ($self->{parsingEnded});

        if ($self->{parsingList})
        {
            if ($tagname eq "p" && $attr->{class} eq "name")
            {
                $self->{isMovie} = 1;
            }
            elsif ($tagname eq "a" && $self->{isMovie} && $attr->{'data-type'} && $attr->{'data-type'}=~ /(film|series)/)
            {
                my $url = $attr->{href};
                if ($url =~ m/\/sr\/1/)
                {
                    $self->{isMovie} = 2;
                    $self->{itemIdx}++;
                    $self->{itemsList}[$self->{itemIdx}]->{url} = $self->{rootUrl}.$url;
                }
            }
            elsif ($tagname eq "span" && $attr->{class} && $attr->{class} eq "year")
            {
                $self->{isYear} = 1;
            }
        }
        else
        {
            if ($tagname eq 'h1')
            {
                $self->{insideTitle} = 1;
            }
            elsif ($tagname eq "span")
            {
                if ($attr->{style} eq "color: #666; font-size: 13px")
                {
                    $self->{insideOriginal} = 1;
                }
            }
            elsif ($tagname eq "div" && $attr->{class} =~ /filmSynopsis/)
            {
                $self->{insideSynopsis} = 1;
            }
            elsif ($tagname eq "a")
            {
                if ($attr->{href} =~ m/\/level\/10\/m\_act\%5Byear\%5D/)
                {
                    $self->{insideDate} = 1;
                }
                elsif ($attr->{href} =~ m/\/movies\/country/)
                {
                    $self->{isCountry} = 1;
                }
                elsif ($self->{insideActorList} && $attr->{itemprop} && $attr->{itemprop} eq "actor")
                {
                    $self->{isActors} += 1;
                    $self->{insideActors} = 1;
                }
                elsif ($attr->{href} =~ m/\/name\//)
                {
                    $self->{isDirector} = 3 if ($self->{isDirector} eq 2);
                }
                elsif ($attr->{href} =~ m/\/movies\/genre/)
                {
                    $self->{isGenre} = 1;
                }
            }
            elsif ($tagname eq "div" && $attr->{class} =~ /rowDark/)
            {
                $self->{isDirector} = 1;
                $self->{isTime} = 1;
            }
            elsif ($tagname eq "td")
            {
                if ($attr->{style} eq "vertical-align: top; height: 15px" && $attr->{align} eq "right" && $self->{isActors} >= 0)
                {
                    $self->{isActors} += 1;
                    $self->{insideActors} = 1;
                }
            }
            elsif ($tagname eq "img" && $attr->{class} =~ /film-poster/)
            {
                if ($attr->{src} =~ /^\/\//)
                {
                    $self->{curInfo}->{image} = "https:".$attr->{src};
                }
            }
        }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        $origtext =~ s/[\r\n]//g;
        $origtext =~ s/^\s*//;
        $origtext =~ s/\s*$//;
        return if $origtext eq "";

        return if ($self->{parsingEnded});

        if ($self->{parsingList})
        {
            if ($self->{isMovie} eq 2)
            {
                my ($title, $date);
                $self->{itemsList}[$self->{itemIdx}]->{title} = $origtext;
                $self->{isMovie} = 0;
            }
            elsif ($self->{isYear})
            {
                $self->{itemsList}[$self->{itemIdx}]->{date} = $origtext;
                $self->{isYear} = 0;
            }
        }
        else
        {
            if ($origtext =~ m/В\s*главных\s*ролях/)
            {
                $self->{insideActorList} = 1;
            }
            elsif ($origtext =~ m/Роли\s*дублировали/)
            {
                $self->{insideActorList} = 0;
            }
            elsif ($self->{insideTitle})
            {
                if ($origtext =~ m/(.*) *\([^0-9]*([0-9]*)\)/)
                {
                    $self->{curInfo}->{title} = $1;
                    $self->{curInfo}->{date} = $2;
                }
                else
                {
                    $self->{curInfo}->{title} = $origtext;
                }
                $self->{insideTitle} = 0;
            }
            elsif ($self->{insideOriginal})
            {
                $origtext =~ s/^\s+//;
                $self->{curInfo}->{original} = $origtext;
                $self->{insideOriginal} = 0;
            }
            elsif ($self->{insideDate})
            {
                $self->{curInfo}->{date} = $origtext;
                $self->{insideDate} = 0;
            }
            elsif ($self->{isCountry})
            {
                $self->{curInfo}->{country} .= ", " if $self->{curInfo}->{country};
                $self->{curInfo}->{country} .= $origtext;
                $self->{isCountry} = 0;
            }
            elsif ($self->{isDirector} eq 3)
            {
                $self->{curInfo}->{director} .= ", " if $self->{curInfo}->{director};
                $self->{curInfo}->{director} .= $origtext;
                $self->{isDirector} = 2;
            }
            elsif ($self->{insideActors})
            {
                if ($self->{isActors} == 1)
                {
                    $self->{curInfo}->{actors} = $origtext;
                }
                elsif ($self->{isActors} > 1)
                {
                    if ($origtext eq "...")
                    {
                        $self->{isActors} = -1;
                    }
                    else
                    {
                        $self->{curInfo}->{actors} = $self->{curInfo}->{actors}.", ".$origtext;
                    }
                }
                $self->{insideActors} = 0;
            }
            elsif ($self->{insideSynopsis} == 1)
            {
                $self->{curInfo}->{synopsis} = $origtext;
                $self->{insideSynopsis} = 2;
            }
            elsif ($self->{isTime} == 1 || $self->{isDirector} == 1)
            {
                $self->{isDirector} = 0;
                $self->{isTime} = 0;
                $self->{isTime} = 2 if ($origtext =~ /время/i);
                $self->{isDirector} = 2 if ($origtext =~ /режиссер/i);
            }
            elsif ($self->{isTime} eq 2)
            {
                if ($origtext =~ /(\d+)\s*ч\s*(\d+)\s*мин.*/)
                {
                    $self->{curInfo}->{time} = ($1*60) + $2;
                }
                elsif ($origtext =~ /(\d+)\s*мин/)
                {
                    $self->{curInfo}->{time} = $1;
                }
                $self->{isTime} = 0;
            }
            elsif ($self->{isGenre})
            {
                $self->{curInfo}->{genre} .= ", " if $self->{curInfo}->{genre};
                $self->{curInfo}->{genre} .= $origtext;
                $self->{isGenre} = 0;
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

        if ($self->{parsingList})
        {
            # Your code for processing search results here
        }
        else
        {
            if ($tagname eq "tr" && $self->{isGenre} != 0)
            {
                $self->{isGenre} = 0;
            }
            elsif ($tagname eq "td")
            {
                $self->{insideActorList} = 0;
            }
        }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{hasField} = {
                              title => 1,
                              date => 1,
                              director => 0,
                              actors => 0,
        };

        $self->{rootUrl} = "https://www.kinopoisk.ru";

        return $self;
    }

    sub getName
    {
        return "Kinopoisk";
    }

    sub getAuthor
    {
        return 'Nazarov Pavel - Kerenoc';
    }

    sub getLang
    {
        return 'RU';
    }

    sub getCharset
    {
        my $self = shift;
        return "windows-1251";
    }

    sub getSearchCharset
    {
        my $self = shift;
        return "windows-1251";
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;
        return $self->{rootUrl}."/index.php?kp_query=$word";
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;
        return $url if $url =~ /^https*:/;
        return $self->{rootUrl} . $url;
    }

    sub getDefaultPictureSuffix
    {
        return '.webp';
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{parsingEnded} = 0;

        $self->{isInfo} = 0;
        $self->{isMovie} = 0;
        $self->{isYear} = 0;
        $self->{isDirector} = 0;
        $self->{isActors} = 0;
        $self->{isTime} = 0;
        $self->{isGenre} = 0;
        $self->{isCountry} = 0;
        $self->{curName} = undef;
        $self->{curUrl} = undef;
        $self->{insideActorList} = 0;

        return $html;
    }
}

1;
