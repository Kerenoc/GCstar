package GCPlugins::GCfilms::GCOFDb;

###################################################
#
#  Copyright 2005-2010 Tian
#  Copyright 2017-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCfilms::GCfilmsCommon;

{
    package GCPlugins::GCfilms::GCPluginOFDb;

    use base qw(GCPlugins::GCfilms::GCfilmsPluginsBase);

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        if ($self->{parsingPlot})
        {
            if ($tagname eq 'h4')
            {
                $self->{insideSynopsis} = 1;
            }
        }
        elsif ($self->{parsingList})
        {
            if ($tagname eq "a")
            {
                if ($attr->{href} =~ m|/film/[0-9]*,|)
                {
                    $self->{isTitle}    = 1;
                    $self->{isInfo}     = 0;
                    $self->{itemIdx}++;
                    $self->{itemsList}[ $self->{itemIdx} ]->{url} = $attr->{href};
                }
            }
            elsif ($tagname eq "span" && $attr->{class} =~ /tooltipster/ && $self->{isTitle})
            {
                $self->{isTitle} = 2;
            }
        }
        else
        {
            if ($tagname eq 'h1' && $attr->{itemprop} eq 'name')
            {
                $self->{insideName} = 1;
            }
            elsif ($tagname eq 'span' && $attr->{itemprop} eq 'genre')
            {
                $self->{insideGenre} = 1;
            }
            elsif ($tagname eq 'span' && $attr->{itemprop} eq 'director')
            {
                $self->{insideDirector} = 1;
            }
            elsif ($tagname eq 'li' && $attr->{itemprop} eq 'actor')
            {
                $self->{insideActors} = 1;
            }
            elsif ($tagname eq "dt" && $attr->{class} =~ /bluegrey/)
            {
                $self->{insideInfosNames} = 1;
            }
            elsif ($tagname eq "img")
            {
                if ($attr->{itemprop} eq "image")
                {
                    $self->{curInfo}->{image} = "https://www.ofdb.de/".$attr->{src}
                        if !$self->{curInfo}->{image};
                }
                elsif ($attr->{src} =~ m|img\.ofdb\.de/film/[0-9]+/[0-9]*.jpg|)
                {
                    $self->{curInfo}->{image} = $attr->{src}
                      if !$self->{curInfo}->{image};
                }
            }
            elsif ($tagname eq "div" && $attr->{class} eq "progress-bar")
            {
                    ($self->{curInfo}->{ratingpress} = $attr->{'aria-valuenow'}) =~ s/\..*//;
            }
            elsif ($tagname eq "a")
            {
                if ($attr->{href} =~ m/\/erscheinungsjahr\/([0-9]{4})/)
                {
                    $self->{curInfo}->{date} = $1;
                }
                elsif ($attr->{href} =~ m|plot|)
                {
                    $self->{curInfo}->{nextUrl} = $attr->{href};
                }
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

        if ($tagname eq "dd")
        {
            $self->{insideCountry} = 0;
        }
        elsif ($self->{parsingPlot} && $self->{insideSynopsis} && $tagname eq 'div')
        {
            $self->{insideSynopsis} = 0;
        }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        $origtext =~ s/[\n\r\t]*//g;
        $origtext =~ s/^\s*//;
        $origtext =~ s/\s*$//;

        return if ! $origtext;

        if ($self->{parsingPlot} && $self->{insideSynopsis})
        {
            $origtext =~ s/\.\.\.$//;
            $origtext =~ s/^\.\.\. //;
            $self->{curInfo}->{synopsis} .= $origtext . " ";
        }
        elsif ($self->{parsingList})
        {
            if ($self->{isTitle} eq 2)
            {
                $self->{itemsList}[ $self->{itemIdx} ]->{"title"} = $origtext;
                $self->{isTitle}    = 0;
                $self->{isInfo}     = 1;
            }
            elsif (($self->{isInfo}) && ($origtext =~ m/(\d{4})/))
            {
                $self->{itemsList}[ $self->{itemIdx} ]->{date} = $1;
                $self->{isInfo} = 0;
            }
        }
        else
        {
            if ($self->{insideName})
            {
                $self->{curInfo}->{title} = $origtext if !$self->{curInfo}->{title};
                $self->{curInfo}->{title} =~ s/\([0-9]*\)//;
                $self->{insideName} = 0;
            }
            elsif ($self->{insideInfosNames})
            {
                $self->{insideOrig}     = 1 if $origtext =~ m/Originaltitel:/;
                $self->{insideCountry}  = 1 if $origtext =~ m/Herstellungsland:/;
                $self->{insideInfosNames} = 0;
            }
            elsif ($self->{insideCountry})
            {
                $self->{curInfo}->{country} .= ', ' if $self->{curInfo}->{country};
                $self->{curInfo}->{country} .= $origtext;
            }
            elsif ($self->{insideOrig})
            {
                $self->{curInfo}->{original} = $origtext;
                $self->{insideOrig}          = 0;
            }
            elsif ($self->{insideDirector})
            {
                $self->{curInfo}->{director} .=
                    $self->{curInfo}->{director}
                    ? ', ' . $origtext
                    : $origtext;
                $self->{insideDirector} = 0;
            }
            elsif ($self->{insideActors})
            {
                push @{$self->{curInfo}->{actors}}, [$origtext]
                    if $self->{actorsCounter} < $GCPlugins::GCfilms::GCfilmsCommon::MAX_ACTORS;
                $self->{actorsCounter}++;
                $self->{insideActors} = 0;
            }
            elsif ($self->{insideGenre})
            {
                push @{$self->{curInfo}->{genre}}, [$origtext];
                $self->{insideGenre} = 0;
            }
        }
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless($self, $class);

        $self->{hasField} = {
            title    => 1,
            date     => 1,
            director => 0,
            actors   => 0,
        };

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{parsingPlot} = 0;
        $self->{parsingPlot} = 1 if $self->{loadedUrl} =~ m|/plot/$|;
        $self->{insideSynopsis} = 0;

        return $html;
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        # if $word looks like an EAN, do a EAN search, otherwise title search
        my $kat = ($word =~ /^[\dX]{8}[\dX]*$/) ? "EAN" : "Titel";
        return "https://www.ofdb.de/suchergebnis/?$word";
        return "https://www.ofdb.de/view.php?page=suchergebnis&Kat=$kat&SText=$word";
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;
        utf8::decode($url);
        return $url;
    }

    sub getCharset
    {
        my $self = shift;

        return "UTF-8";
    }

    sub getSearchCharset
    {
        my $self = shift;

        return "UTF-8";
    }

    sub getName
    {
        return "OFDb.de";
    }

    sub getAuthor
    {
        return 'MeV - Kerenoc';
    }

    sub getLang
    {
        return 'DE';
    }

}

1;
